# Recsysart.ru: RecSys & MLOps playground for fun, challenge and experience

![main_screen.gif](https://recsysart.ru/preview/preview.gif)

See our online app [here](https://recsysart.ru)

Main goals of the application:

- Online recommendations with various recommender algorithms on Kion movie dataset, including competition 4th place solution
- Reports on all of our experiments with the models: metrics and visual analysis

Main goals of the project:

- Easy, well-documented and fully reproducible experiments with various recsys models, including 2-stage models with time-dependent features. Support for both cross-validation and visual analysis


# Project components

![services_scheme.jpg](Docker/nginx/static/images/services_scheme.jpg)

The project can be divided into two primary components:

- Server-side, consisting of a set of docker containers
- Local-side
    - DVC pipelines for data processing, feature engineering and model experiments
    - Data parser for collecting additional data, such as posters and extrenal items IDs for IMDB, Kinopoisk and Kinoposik HD services

The project architecture enable developers conduct experiments locally. The server-side infrastructure acts as a centralized storage for keeping artifacts and providing environment for visual testing and models evaluation.

## Server components

The server side of app described in `docker-compose.yaml` and includes following services:

| Service | Source | Docker compose service name | Ports | Network (bridge) |
| --- | --- | --- | --- | --- |
| Frontend (recsys + EDA) | kion_recsys/src/frontend kion_recsys/Docker/frontend | frontend | 80 | s3, app, proxy |
| Frontend (visual analysis) | kion_recsys/Docker/voila | voila | 8010 | postgres, proxy |
| Backend (API) | kion_recsys/src/backend kion_recsys/Docker/model_service | model_service | 8003 | s3, app, postgres |
| Nginx proxy server | kion_recsys/Docker/nginx | nginx | 9000, 9001, 5050 | s3, proxy |
| MLflow | kion_recsys/Docker/mlflow | mlflow | 5000 | postgres,  s3, proxy |
| PostgreSQL | kion_recsys/src/db | postgres | 5432 | postgres |
| PgAdmin  | Only docker-compose.yaml | pgadmin | 5050 | postgres |
| Minio | Only docker-compose.yaml | minio | 9000, 9001 | s3 |

- The **frontend** component was developed using Streamlit and Voila frameworks. It is designed as a multi-page application, comprising the following pages:
    - Online recommendation page
        
        Interactive interface for making movie recommendations. Users can select their viewing history, input personal features, and choose a specific recommendation algorithm.
        
    - Exploratory data analysis (EDA) page
        
        Key insight about MTS Kion dataset
        
    - Recommendations and experiments visual analysis page
        
        Visual analysis of the pre-defined users groups recommendations and experiment logs.
        
    - Additional info page
        
        More model training details, validation schemes, etc.
        
- The **backend** component of the project is responsible for managing the project's API, providing model serving and exploratory data analysis (EDA) functionalities. Based on FastAPI.
- **MLflow**  is responsible for logging and storing artefacts generated during model training. Experiment logs are stored in **PostgreSQL**, model weights in **Minio** S3-like storage.
- **PostgreSQL** is also used to store additional model features and pre-calculated data for EDA. Administered by **PgAdmin**
- **Minio**  is also used as remote S3-like storage for **DVC** pipelines results and parsed posters.

## Local components

### DVC data pipelines

- Data preparation pipeline (for both static and time-based features used in cross-validation): `kion_recsys/dvc.yaml`
- Updating database pipeline: `kion_recsys/workflow/prod/dvc.yaml`

DVC data pipelines are always run before any experiments with models. This ensures that any code changes in data preparation or feature engineering generate up-to-date data for models training.

### Training pipelines (Experiments with recommender models)

All experiments with recommender models are managed solely by configs and provide the following options:

- **Cross-validation** with specified metrics and common validation scheme: `kion_recsys/src/experiemnts/cross_val_config.yaml`. All metrics and model parameters are logged to Mlflow
- `save_for_visual_analysis`option which saves recommended items from model for comparison with other models
- `save_to_mlflow` option which saves trained model to Mlflow and registers in Model Registry so that the model can be run online
- `skip_metrics` to skip cross-validation if necessary
- `report_filename` option which specifies the local file to write report

Full experiments functionality is available for both one-stage and two-stage models. Here are examples of main configs and reports which manage experiments:

- Two-stage models experiment **config** example: `kion_recsys/src/experiments/two_stage/v0_competition_winner.yaml`
- One-stage models experiment **config** example: `kion_recsys/src/experiments/baselines/cosine.yaml`
- Local experiment **report** example with link to Mlflow run: `kion_recsys/reports/cosine_metrics.csv`
- **Visual analysis** example: see [here](https://recsysart.ru/voila/)

Models that were saved to Mlflow during experiments can be served for online recommendations if specified in backend config on Server side. Example of the online models config: `kion_recsys/src/backend/prod_api_models.yaml`. You need to specify which config to use in the env variable `ONLINE_MODELS_DESCRIPTIONS_PATH` in your .env file.

### Data parser

Posters parser based on the Kinopoisk's API for the MTS Kion dataset.<br>
Features:
- Parse poster's URL along with kinopoisk, kinopoisk HD and IMDB IDs into CSV file. Search based on original and translated titles with release year
- Download poster images
- Upload result CSV table to PostgreSQL
- Upload images to Minio S3-like storage

# Project set up

## Server-side

### Virtual machine

- Install `docker` and `docker compose`. App was tested with Docker 23.0.6 and Docker Compose 2.17.3 under Ubuntu 20.04.6 LTS.
- Fill `.env` file, use template `.env.example`
- `sudo docker compose up —build -d`

### Minio

- Visit `http://<hostname>:9001`
- Create `arts` and `dvc` buckets

### PostgreSQL

- Visit `http://<hostname>:5050`
- Create `features_db` and `mlflow_db` databases

### Posters parsing (additional step)

Posters were not initially included in Kion dataset, so you need to parse them separately.

Check details in `kinopoisk_parser` repository

### Web app

Visit `http://<hostname>:80` 

## Local-side

### Environment setup

Create environment

```bash
pip install virtualenv
virtualenv venv -p python3.8
source venv/bin/activate
```

Install packages

```bash
pip3 install poetry==1.1.15 requests==2.26.0
poetry install --no-dev
```

### Model training

**Locally:**

- Write config for baseline model experiment.

Specify `save_to_mlflow = True` to use model online. Specify `skip_metrics= True`to skip cross-validation if needed. `kion_recsys/src/experiments/baselines/current.yaml`

- Run `make exp_base` to run experiment, train model and log it to Mlflow.

Alternatively you can run experiment for a two-stage model with config path `kion_recsys/src/experiments/two_stage/current.yaml` and command `make exp_two_stage`

- Run `make prod` to update server database if necessary

**Server side:**

- Specify you model registry model name and version in backend models config: `kion_recsys/src/backend/empty_api_models.yaml`
- Run `sudo docker compose up —-build -d` to load new model to both API and frontend.

**New model online!**

# Additional features of the app

### Report on all of our experiments
You can read about our research for the ultimate movie recommendation algorithm on Kion dataset [here](https://recsysart.ru/Метрики%20и%20визуальный%20анализ). We share both summary of the research, cross-validation results on key metrics and visual analysis tool. This is the preview for the latter:

![visual analysis](https://recsysart.ru/preview/visual_preview.gif)

### Exploratory data analysis report
We share some very helpful insights from EDA on Kion dataset [here](https://recsysart.ru/Анализ%20данных%20датасета%20Kion). 
  
![eda](https://recsysart.ru/preview/eda.gif)


# Links

- [Article about the MTS Kion dataset from ACM  RecSys CARS workshop](https://arxiv.org/abs/2209.00325)
- [Link to the competition at ods.ai](https://ods.ai/tracks/recsys-course2021/competitions/competition-recsys-21)
- [Video description of the original solution at the MTS RecSys meetup](https://www.youtube.com/watch?v=rTjm0IJNpsQ)
