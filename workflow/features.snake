# This is part of the dvc pipeline and is used for dates config parsing and parallelisation
# Not meant to be run as a standalone pipeline

configfile: "workflow/workflow_config.yaml"

DATES = list(set(config["pipe_dates"]["begin"].values()).union(set(config["features_dates"])))
OUTPUT_TIME_ITEMS_PATHS = [f"data/processed/{date}/item_features.csv" for date in DATES]
OUTPUT_TIME_USER_PATHS = [f"data/processed/{date}/user_features.csv" for date in DATES]
WINDOW_SIZE = config["window_size"]
FILES = config["files"]

rule all:
    input:
        OUTPUT_TIME_ITEMS_PATHS,
        OUTPUT_TIME_USER_PATHS,

rule prepare_all_items_time_based:
    input:
        OUTPUT_TIME_ITEMS_PATHS,

rule prepare_all_users_time_based:
    input:
        OUTPUT_TIME_USER_PATHS,

rule add_item_stats_time_based:
    output:
        "data/processed/{date}/item_features.csv"
    shell:
        "python src/features/add_item_stats_time_based.py {FILES[clean_inter]} {FILES[clean_items]} {FILES[clean_users]} {output} --date {wildcards.date} --window {WINDOW_SIZE}"

rule add_user_stats_time_based:
    output:
        "data/processed/{date}/user_features.csv"
    shell:
        "python src/features/add_user_stats_time_based.py {FILES[clean_inter]} {FILES[clean_users]} {FILES[clean_items]} {output} --date {wildcards.date}"
