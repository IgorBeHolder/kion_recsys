import logging
import os
import sys

import pandas as pd
from dotenv import load_dotenv
from fastapi import FastAPI
from sqlalchemy import create_engine

from src.backend.tools import (
    ItemIds,
    UserData,
    get_features_for_item_ids,
    get_models,
    get_reco_model_descriptions_from_yaml,
)

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="[%(asctime)s] [%(name)s] [%(levelname)s] > %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger("API")
logger.info("Starting app...")

load_dotenv()

engine = create_engine(
    f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}'
    f'@db:5432/{os.getenv("POSTGRES_FEATURE_DB")}'
)

reco_model_descriptions = get_reco_model_descriptions_from_yaml(
    str(os.getenv("ONLINE_MODELS_DESCRIPTIONS_PATH"))
)
models = get_models(reco_model_descriptions, engine=engine)
logger.info("Loaded recommender models")

app = FastAPI()

# ------------------------------ EDA endpoints ------------------------------ #


@app.get("/get_top_100_items")
async def get_top_100_items():
    sql_template = """
    SELECT
        CASE
            WHEN release_year = 0 THEN title
            ELSE CONCAT(title, ', ', release_year)
        END AS title,
        content_type,
        COUNT(i.item_id) AS cnt
    FROM interactions as i
    INNER JOIN item_features as itf
    ON i.item_id = itf.item_id
    GROUP BY 1, content_type
    ORDER BY cnt DESC
    LIMIT 100
    """
    output_df = pd.read_sql_query(sql=sql_template, con=engine)
    return output_df.to_json(orient="index")


@app.get("/get_top_100_weekly_stats")
async def get_top_100_weekly_stats(content_type: str):
    sql_template = f"""
    SELECT
        CASE
            WHEN release_year = 0 THEN title
            ELSE CONCAT(title, ', ', release_year)
        END AS title,
        cnt,
        week,
        item_id
    FROM weekly_interactions
    WHERE
        title IN
            (
                SELECT
                    title
                FROM weekly_interactions
                WHERE content_type = '{content_type}'
                GROUP BY title
                ORDER BY SUM(cnt) DESC
                LIMIT 100
            )
    ORDER BY title, week DESC
    """
    output_df = pd.read_sql_query(sql=sql_template, con=engine)
    return output_df.to_json(orient="index")


@app.get("/get_statistic_per_county_and_genre")
async def get_statistic_per_county_and_genre():
    sql_template = """
    SELECT
        CASE
            WHEN release_year = 0 THEN title
            ELSE CONCAT(title, ', ', release_year)
        END AS title,
        main_country,
        main_genre,
        content_type,
        COUNT(i.item_id) AS cnt
    FROM interactions as i
    INNER JOIN item_features as itf
    ON i.item_id=itf.item_id
    WHERE
        main_genre != 'None' AND
        main_country != 'None'
    GROUP BY 1, main_country, main_genre, content_type
    ORDER BY cnt DESC
    """
    output_df = pd.read_sql_query(sql=sql_template, con=engine)
    return output_df.to_json(orient="index")


# --------------------------------------------------------------------------- #


@app.get("/get_films_title_with_year")
async def get_films_title_with_year():
    sql_template = """
        SELECT
            item_id,
            CASE
                WHEN release_year = 0 THEN title
                ELSE CONCAT(title, ', ', release_year)
            END AS title_with_year
        FROM item_features
        WHERE watched_in_all_time >= 100
    """
    output_df = pd.read_sql_query(sql=sql_template, con=engine)
    output_df = output_df.rename(columns={"title_with_year": "title"}).sort_values(
        by="title"
    )
    return output_df.to_json(orient="index")


@app.get("/get_models_list_for_ui")
async def get_models_list_for_ui():
    models_list = []
    for model_name, model in models.items():
        models_list.append((model.ui_order, model.ui_description, model_name))
    models_ordered = [
        (model_description, model_name)
        for (_, model_description, model_name) in sorted(models_list)
    ]
    return models_ordered


@app.post("/get_features_for_item_ids")
async def get_features_for_item_id(item_ids: ItemIds):
    output_df = get_features_for_item_ids(item_ids.ids, engine=engine)
    return output_df.to_json(orient="index")


@app.post("/recommend_with_model/{model_name}")
async def recommend_with_model(
    model_name: str, user_data: UserData, num_recos: int = 10
):
    model = models[model_name]
    recos = model.recommend(user_data, num_recos)
    return [int(x) for x in recos["item_id"].values]


# Check environmet variables for AWS access
if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    logger.critical("Environment variables for S3 are not specified")
    sys.exit(1)
