# pylint: disable=bare-except

import logging
import os
import typing as tp

import mlflow
import numpy as np
import pandas as pd
import yaml
from dotenv import load_dotenv
from mlflow.exceptions import RestException
from pydantic import BaseModel
from rectools import Columns
from rectools.dataset import Dataset, IdMap, Interactions
from sqlalchemy.engine import Engine

from src.models.heuristics import rank_on_cosine
from src.models.two_stage_blocks import process_features_for_inference
from src.models.two_stage_recommender import FirstStageModel, TwoStageRecommender

logger = logging.getLogger("API Recommenders")

load_dotenv()


class ItemIds(BaseModel):
    ids: tp.List[int]


class UserFeatures(BaseModel):
    sex: str = "sex_unknown"
    age: str = "age_unknown"
    income: str = "income_unknown"
    kids_flg: bool = False


class UserData(BaseModel):
    item_ids: ItemIds
    features: UserFeatures


class RecoModelDescription(BaseModel):
    model_type: str
    mlflow_model_name: str
    mlflow_model_version: str
    ui_model_decription: str
    ui_order: int = 0


def get_reco_model_descriptions_from_yaml(
    yaml_config_path: str,
) -> tp.Dict[str, RecoModelDescription]:
    with open(yaml_config_path, "r", encoding="utf-8") as file:
        config = yaml.load(file, Loader=yaml.SafeLoader)
    if config["models"] is not None:
        model_descriptions = {
            key: RecoModelDescription.parse_obj(value)
            for key, value in config["models"].items()
        }
    else:
        return {}
    return model_descriptions


def get_features_for_item_ids(item_ids: tp.List[int], engine: Engine) -> pd.DataFrame:
    item_ids_str = ", ".join(str(idx) for idx in item_ids)
    sql_template = f"""
        SELECT *
        FROM {os.getenv("POSTGRES_FEATURE_TABLE")}
        WHERE item_id IN ({item_ids_str})
    """
    output_df = pd.read_sql_query(sql=sql_template, con=engine)
    return output_df


def get_items_to_recommend(engine: Engine, watch_cnt_limit: int = 10) -> np.ndarray:
    sql_template = f"""
        SELECT item_id
        FROM item_features
        WHERE watched_in_all_time > {watch_cnt_limit}
    """
    item_ids = pd.read_sql_query(sql=sql_template, con=engine)["item_id"].values
    return item_ids


class Model:
    """API model from mlflow with pyfunc flavour"""

    def __init__(
        self,
        model_name: str,
        model_stage: str,
        engine: Engine,
        ui_description: str,
        ui_order: int,
    ):
        """Loading model from mlflow

        Parameters
        ----------
        model_name : str
            Name of the model in Model Registry
        model_stage : str
            Model stage or model version
        """
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")
        run_id = self.model._model_meta.run_id
        try:
            with open(
                mlflow.artifacts.download_artifacts(
                    run_id=run_id, artifact_path="heuristics.yaml"
                ),
                encoding="utf-8",
            ) as yamlfile:
                self.heuristics = yaml.load(yamlfile, Loader=yaml.SafeLoader)
        except:  # noqa
            self.heuristics = None

        unwrapped_model = self.model.unwrap_python_model()
        self.rectools_model = unwrapped_model.model
        self.rectools_model.is_fitted = True
        self.item_id_map = unwrapped_model.item_id_map
        self.engine = engine
        self.ui_description = ui_description
        self.ui_order = ui_order
        logger.info(f"Loaded {model_name}/{model_stage} for API")

    def recommend(
        self,
        user_data: UserData,
        num_recos: int,
        items_to_recommend: tp.Optional[np.ndarray] = None,
    ) -> pd.DataFrame:

        interactions_df = pd.DataFrame({"item_id": user_data.item_ids.ids})
        interactions_df[Columns.Weight] = 1
        interactions_df[Columns.Datetime] = "2022-02-02"
        interactions_df[Columns.User] = "user"
        user_id_map = IdMap.from_values(interactions_df[Columns.User])
        interactions = Interactions.from_raw(
            interactions_df, user_id_map, self.item_id_map
        )
        dataset = Dataset(user_id_map, self.item_id_map, interactions)

        if (
            self.heuristics is not None
            and "Heuristic.RANK_ON_COSINE" in self.heuristics
        ):
            num_dummies = self.heuristics["Heuristic.RANK_ON_COSINE"]
            recos = self.rectools_model.recommend(
                users=user_id_map.external_ids,
                dataset=dataset,
                k=num_recos * 2,
                filter_viewed=True,
                items_to_recommend=items_to_recommend,
                add_rank_col=True,
            )

            required_items = list(recos[Columns.Item].unique()) + list(
                interactions_df[Columns.Item].unique()
            )
            item_features = get_features_for_item_ids(
                required_items, engine=self.engine
            )
            dummy_country_cols = [
                col_name
                for col_name in item_features.columns
                if col_name.startswith("country_dummy_")
            ]

            dummy_genre_cols = [
                col_name
                for col_name in item_features.columns
                if col_name.startswith("genre_dummy_")
            ]

            dummy_feature_cols = dummy_country_cols + dummy_genre_cols

            if num_dummies is not None:
                dummy_feature_cols = dummy_feature_cols[:num_dummies]
            dummy_features = item_features[["item_id"] + dummy_feature_cols]

            interaction_genres = interactions_df.merge(
                dummy_features, on="item_id", how="left"
            )
            user_vector = interaction_genres[dummy_feature_cols].mean()
            logger.info(f"user_vector: {user_vector}")

            recos = recos.merge(dummy_features, on="item_id", how="left")
            recos["genres_dummy_dot"] = recos[dummy_feature_cols] @ user_vector.T
            recos["genres_dummy_norms"] = np.linalg.norm(
                recos[dummy_feature_cols].values, axis=1
            )
            recos["genres_dummy_cosine"] = (
                recos["genres_dummy_dot"] / recos["genres_dummy_norms"]
            ) / np.linalg.norm(user_vector.values)
            recos = rank_on_cosine(recos, num_recos=num_recos, cosine_rank_weight=2)
            return recos

        # Recommending without heristics
        recos = self.rectools_model.recommend(
            users=user_id_map.external_ids,
            dataset=dataset,
            k=num_recos,
            filter_viewed=True,
            items_to_recommend=items_to_recommend,
        )
        return recos


class TwoStageModel:
    def __init__(
        self,
        model_name: str,
        model_stage: str,
        engine: Engine,
        ui_description: str,
        ui_order: int,
    ):
        """Loading model from mlflow

        Parameters
        ----------
        model_name : str
            Name of the model in Model Registry
        model_stage : str
            Model stage or model version
        """
        wrapped_second_stage_model = mlflow.pyfunc.load_model(
            model_uri=f"models:/{model_name}/{model_stage}"
        )
        caboost_second_stage_model = mlflow.catboost.load_model(
            model_uri=f"models:/{model_name}/{model_stage}"
        )
        run_id = wrapped_second_stage_model._model_meta.run_id
        mlflow_client = mlflow.tracking.MlflowClient()
        with open(
            mlflow.artifacts.download_artifacts(
                run_id=run_id, artifact_path="two_stage_config.yaml"
            ),
            encoding="utf-8",
        ) as yamlfile:
            two_stage_cfg = yaml.load(yamlfile, Loader=yaml.SafeLoader)

        run = mlflow_client.get_run(run_id)
        run_data_dict = run.data.to_dictionary()
        registered_first_stage_models = run_data_dict["params"][
            "registered_first_stage_models"
        ]

        first_stage_models = {}
        item_id_map = None
        for registry_model_name in registered_first_stage_models.split(", "):
            mlflow_wrapped_model = mlflow.pyfunc.load_model(
                f"models:/{registry_model_name}"
            )
            unwrapped_model = mlflow_wrapped_model.unwrap_python_model()

            model_prefix_len = len(
                two_stage_cfg["mlflow"]["first_stage_registry_model_prefix"]
            )
            model_name_with_version = registry_model_name[model_prefix_len:]
            model_name, _ = model_name_with_version.split("/")

            first_stage_models[model_name] = unwrapped_model.model
            first_stage_models[model_name].is_fitted = True
            if item_id_map is None:
                item_id_map = unwrapped_model.item_id_map

        first_stage_config = {}
        for first_stage_model_name, model_config in two_stage_cfg[
            "first_stage_config"
        ].items():
            first_stage_config[first_stage_model_name] = FirstStageModel.parse_obj(
                model_config
            )

        two_stage_recommender = TwoStageRecommender(first_stage_config)
        two_stage_recommender.recommenders = first_stage_models
        two_stage_recommender.second_stage_model = caboost_second_stage_model
        two_stage_recommender.is_fitted = True

        self.model: TwoStageRecommender = two_stage_recommender
        self.item_id_map: IdMap = item_id_map
        self.two_stage_config = two_stage_cfg
        self.engine = engine
        self.ui_description = ui_description
        self.ui_order = ui_order
        logger.info(f"Loaded {model_name}/{model_stage} for API")

    def _calc_features_online(
        self,
        first_stage_candidates: pd.DataFrame,
        dataset: Dataset,
        user_features: UserFeatures,
        for_inference: bool = True,
    ) -> pd.DataFrame:
        result_candidates = first_stage_candidates

        item_features = get_features_for_item_ids(
            first_stage_candidates[Columns.Item].unique(), engine=self.engine
        )
        selected_cols = []

        if self.two_stage_config["features"]["items_static"] is not None:
            selected_cols.extend(self.two_stage_config["features"]["items_static"])

        if self.two_stage_config["features"]["items_time_based"] is not None:
            selected_cols.extend(self.two_stage_config["features"]["items_time_based"])

        if len(selected_cols) > 0:
            result_candidates = result_candidates.join(
                item_features.set_index(Columns.Item)[selected_cols],
                on=Columns.Item,
                how="left",
            )

        if self.two_stage_config["features"]["users_static"] is not None:
            user_features_dict = user_features.dict()
            for col in self.two_stage_config["features"]["users_static"]:
                result_candidates[col] = user_features_dict[col]

        # All user time based cols are just interactions count, so we calc them accordingly:
        if self.two_stage_config["features"]["users_time_based"] is not None:
            for col in self.two_stage_config["features"]["users_time_based"]:
                result_candidates[col] = dataset.interactions.df.shape[0]

        if for_inference:
            process_features_for_inference(result_candidates)

        if self.two_stage_config["features"]["category_cols"] is not None:
            result_candidates[
                self.two_stage_config["features"]["category_cols"]
            ] = result_candidates[
                self.two_stage_config["features"]["category_cols"]
            ].astype(
                "category"
            )

        logging.info("Prepared candidates for second_stage model")

        return result_candidates

    def recommend(
        self,
        user_data: UserData,
        num_recos: int,
        items_to_recommend: tp.Optional[
            np.ndarray
        ] = None,  # pylint: disable=unused-argument
    ) -> pd.DataFrame:
        interactions_df = pd.DataFrame({"item_id": user_data.item_ids.ids})
        interactions_df[Columns.Weight] = 1
        interactions_df[Columns.User] = "_default_user"
        interactions_df[Columns.Datetime] = "2022-01-01"

        user_id_map = IdMap.from_values(interactions_df[Columns.User])
        interactions = Interactions.from_raw(
            interactions_df, user_id_map, self.item_id_map
        )
        dataset = Dataset(user_id_map, self.item_id_map, interactions)

        recos = self.model.recommend(
            users=user_id_map.external_ids,
            dataset=dataset,
            k=num_recos,
            filter_viewed=True,
            items_to_recommend=items_to_recommend,
            calc_features_func=self._calc_features_online,
            calc_features_func_kwargs={"user_features": user_data.features},
        )
        return recos


def get_models(
    models_config: tp.Dict[str, RecoModelDescription], engine: Engine
) -> tp.Dict[str, tp.Union[Model, TwoStageModel]]:

    reco_models: tp.Dict[str, tp.Union[Model, TwoStageModel]] = {}
    for model_name, model_decription in models_config.items():
        model_type, mlflow_model_name, model_stage, ui_description, ui_order = (
            model_decription.model_type,
            model_decription.mlflow_model_name,
            model_decription.mlflow_model_version,
            model_decription.ui_model_decription,
            model_decription.ui_order,
        )
        try:
            if model_decription.model_type == "one_stage":
                reco_models[model_name] = Model(
                    mlflow_model_name,
                    model_stage,
                    engine=engine,
                    ui_description=ui_description,
                    ui_order=ui_order,
                )
            elif model_decription.model_type == "two_stage":
                reco_models[model_name] = TwoStageModel(
                    mlflow_model_name,
                    model_stage,
                    engine=engine,
                    ui_description=ui_description,
                    ui_order=ui_order,
                )
            else:
                raise ValueError()
        except RestException:
            logger.error(
                f"""Failed to load model {model_name} from mlflow registered model
                {mlflow_model_name} with stage {model_stage}"""
            )
        except ValueError:
            logger.error(f"Model type {model_type} for model {model_name} is not known")
    return reco_models
