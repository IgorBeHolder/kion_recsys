import logging

import click
import pandas as pd


def add_user_stats(users_df: pd.DataFrame) -> pd.DataFrame:
    """
    Computes user watches stats for particular interactions date split
    and adds them to users dataframe.

    Args:
    - users_df (pd.DataFrame): pd.DataFrame with users features.

    Returns:
    - DataFrame: updated users Dataframe
    """
    users_df["age"] = users_df["age"].fillna("age_unknown")
    users_df["income"] = users_df["income"].fillna("income_unknown")
    users_df["sex"] = users_df["sex"].fillna("sex_unknown")
    users_df["kids_flg"] = users_df["kids_flg"].fillna(False)
    users_df["kids_flg"] = users_df["kids_flg"].astype(bool)
    return users_df


@click.command()
@click.argument("users_input_path", type=click.Path())
@click.argument("users_output_path", type=click.Path())
def add_and_save_user_stats(
    users_input_path: str,
    users_output_path: str,
) -> None:
    """
    Adds static features to users dataframe and saves it to a specified path.

    Args:
    users_input_path (str): A string with the path to the input CSV file containing the users data.
    users_output_path (str): A string with the path to the output CSV file where the updated users
        dataframe with the added features will be saved.

    Returns:
    - None
    """
    logging.basicConfig(level=logging.INFO)
    logging.info("Adding user static features")

    users_df = pd.read_csv(users_input_path)
    users_df = add_user_stats(users_df)
    users_df.to_csv(users_output_path, index=False)


if __name__ == "__main__":
    add_and_save_user_stats()

# Force to run this part
# snakemake -c1 -r add_user_stats_static
