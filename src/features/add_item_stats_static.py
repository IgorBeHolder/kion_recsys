import logging
from typing import List

import click
import pandas as pd

BOOSTING_FEATURES = [
    "item_id",
    "for_kids",
    "age_rating",
    "release_novelty",
    "genres_min",
    "genres_max",
    "genres_med",
    "countries_max",
    "studios_max",
    "content_type",
]
# BOOSTING_FEATURES = BOOSTING_FEATURES + [f"genre_dummy_{i}" for i in range(10)]


def filter_boosting_features(
    items_df: pd.DataFrame, selected_features: List[str]
) -> pd.DataFrame:
    """
    Filters and returns a subset of the input DataFrame that includes
    only the columns corresponding to boosting features.

    Args:
    - items_df (pd.DataFrame): The input DataFrame to filter. It should have at least the columns
        listed in the selected_features list.
    - selected_features (List[str]): List of target subset features.

    Returns:
    - pd.DataFrame: A new DataFrame containing only the columns corresponding to boosting features.
    """
    dummy_features = [
        feature_name
        for feature_name in items_df.columns.to_list()
        if feature_name.startswith("genre_dummy_")
        or feature_name.startswith("country_dummy_")
    ]
    output = items_df[selected_features + dummy_features]
    return output


@click.command()
@click.argument("items_input_path", type=click.Path())
@click.argument("items_output_path", type=click.Path())
def add_item_stats(
    items_input_path: str,
    items_output_path: str,
) -> None:
    """
    Select static item features from the input DataFrame and save the result to a CSV file.

    Args:
    - items_input_path (str): The path to the input CSV file containing item data.
    - items_output_path (str): The path to save the output CSV file containing item features.

    Returns:
    - None
    """
    logging.basicConfig(level=logging.INFO)
    logging.info("Create static item features")

    items_df = pd.read_csv(items_input_path)
    result = filter_boosting_features(items_df, BOOSTING_FEATURES)
    result.to_csv(items_output_path, index=False)


if __name__ == "__main__":
    add_item_stats()


# Force to run this part
# snakemake -c1 -r add_item_stats_static
