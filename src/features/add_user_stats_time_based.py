import logging
from datetime import datetime
from typing import Union

import click
import pandas as pd


def add_user_stats(
    interactions_df: pd.DataFrame,
    users_df: pd.DataFrame,
    items_df: pd.DataFrame,
    max_date: Union[str, datetime] = "2021-08-22",
) -> pd.DataFrame:
    """
    Computes user watches stats for particular interactions date split
    and adds them to users dataframe.

    Args:
    - interactions_df (pd.DataFrame): A pandas dataframe containing data about user interactions.
    - users_df (pd.DataFrame): A pandas dataframe containing information about users.
    - max_date (datetime or str): The maximum date for the interactions.

    Returns:
    A pandas dataframe containing user watch statistics.
    """
    if isinstance(max_date, str):
        max_date = datetime.strptime(max_date, "%Y-%m-%d")

    # Limit interaction history to prevent data leak
    interactions_df = interactions_df.loc[interactions_df["datetime"] <= max_date]

    # Calculate features
    user_watch_count_all = (
        interactions_df[interactions_df["total_dur"] > 300]
        .groupby(by="user_id")["item_id"]
        .count()
        .rename("user_watch_cnt_all")
        .reset_index()
    )

    user_watch_count_last_14 = (
        interactions_df[
            (interactions_df["total_dur"] > 300)
            & (interactions_df["datetime"] >= (max_date - pd.Timedelta(days=14)))
        ]
        .groupby(by="user_id")["item_id"]
        .count()
        .rename("user_watch_cnt_last_14")
        .reset_index()
    )

    user_watches = user_watch_count_all.merge(
        user_watch_count_last_14, on="user_id", how="outer"
    )

    user_watches = user_watches.fillna(0)
    user_watches = user_watches.astype("int64")
    feature_cols = user_watches.columns
    users_df = users_df.merge(user_watches, on="user_id", how="outer")
    users_df[feature_cols] = users_df[feature_cols].fillna(0)

    # add mean genre dummy features for users
    dummy_feature_cols = [
        col_name
        for col_name in items_df.columns
        if col_name.startswith("genre_dummy_") or col_name.startswith("country_dummy_")
    ]
    dummy_features = items_df[["item_id"] + dummy_feature_cols]
    interaction_genres = interactions_df.merge(dummy_features, on="item_id", how="left")
    user_mean_dummies = users_df[["user_id"]]
    for col_name in dummy_feature_cols:
        user_dummy_mean = interaction_genres.groupby("user_id")[col_name].mean()
        user_mean_dummies = user_mean_dummies.join(
            user_dummy_mean, on="user_id", how="left"
        )
        user_mean_dummies[col_name] = user_mean_dummies[col_name].fillna(0)
    users_df = users_df.merge(user_mean_dummies, on="user_id", how="left")

    return users_df[list(feature_cols) + dummy_feature_cols]


@click.command()
@click.argument("interactions_input_path", type=click.Path())
@click.argument("users_input_path", type=click.Path())
@click.argument("items_input_path", type=click.Path())
@click.argument("items_output_path", type=click.Path())
@click.option("--date", type=str)
def add_and_save_user_stats(
    interactions_input_path: str,
    users_input_path: str,
    items_input_path: str,
    items_output_path: str,
    date: str,
) -> None:
    """
    Calculate timde-depends users features and save the result to a CSV file.

    Args:
    - users_input_path (str): Path to the CSV file with the user data.
    - items_output_path (str): Path to save the CSV file with the item features.
    - date (str): Date string (YYYY-MM-DD format) to define the end of the time.
        window for calculating time-based features.

    Returns:
    - None.
    """
    logging.basicConfig(level=logging.INFO)
    logging.info(f"Adding user time-based features for {date}")

    # Read data
    interactions_df = pd.read_csv(interactions_input_path, parse_dates=["datetime"])
    users_df = pd.read_csv(users_input_path)
    items_df = pd.read_csv(items_input_path)

    # Compute stats and save data
    result = add_user_stats(interactions_df, users_df, items_df, date)
    result.to_csv(items_output_path, index=False)


if __name__ == "__main__":
    add_and_save_user_stats()

# Force to run this part
# snakemake -c1 -r add_user_stats_time_based
