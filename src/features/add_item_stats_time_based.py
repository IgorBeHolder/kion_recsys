import logging
from datetime import datetime
from typing import Callable, Union

import click
import numpy as np
import pandas as pd

# ---------------------------- Helper functions ---------------------------- #


def smooth(
    series: np.ndarray, window_size: int, smoothing_func: Callable
) -> np.ndarray:
    """
    Computes smoothed interaction statistics for an item.

    Args:
    - series (np.ndarray): A 2D numpy array containing the item's interaction statistics.
    - window_size (int): The size of the window to use for smoothing.
    - smoothing_func (callable): A function that computes the smoothing weights.

    Returns:
    - np.ndarray: A 1D numpy array containing the smoothed interaction statistics.
    """
    series = np.array(series)
    ext = np.r_[
        2 * series[0] - series[window_size - 1 :: -1],
        series,
        2 * series[-1] - series[-1:-window_size:-1],
    ]
    weights = smoothing_func(window_size)
    smoothed = np.convolve(weights / weights.sum(), ext, mode="same")
    return smoothed[window_size : -window_size + 1]


def trend_slope(
    series: np.ndarray, window_size: int = 7, smoothing_func: Callable = np.hamming
) -> np.ndarray:
    """
    Computes trend slope for item interactions

    Args:
    - series (np.ndarray): A 2D numpy array containing the item's interaction statistics.
    - window_size (int): The size of the window to use for smoothing.
    - smoothing_func (callable): A function that computes the smoothing weights.

    Returns:
    - np.ndarray: A 1D numpy array containing the smoothed interaction statistics.
    """
    smoothed = smooth(series, window_size, smoothing_func)
    return smoothed[-1] - smoothed[-2]


# -------------------------------------------------------------------------- #


def add_item_watches_stats(
    interactions_df: pd.DataFrame,
    items_df: pd.DataFrame,
    max_date: Union[str, datetime] = "2021-08-22",
    window_size: int = 7,
) -> pd.DataFrame:
    """
    Computes item watches stats for particular interactions date split
    and adds them to item_stats dataframe.

    Args:
    - interactions_df (pd.DataFrame): DataFrame containing user-item interactions
        with columns ['user_id', 'item_id', 'datetime']
    - items_df (pd.DataFrame): DataFrame containing item features with columns ['item_id']
    - max_date(datetime or str): Date until which interactions will be considered.
        Default is '2021-08-22'
    - window_size (int): The size of the rolling window. Default is 7

    Returns:
    - DataFrame containing item features including item watches stats
    """
    if isinstance(max_date, str):
        max_date = datetime.strptime(max_date, "%Y-%m-%d")

    # Limit interaction history to prevent data leak
    interactions_df = interactions_df[interactions_df["datetime"] <= max_date].copy()

    # Calculate trend slope feature
    item_stats = items_df[["item_id"]].set_index("item_id")
    for col in list(range(window_size)):
        watches = interactions_df[
            interactions_df["datetime"]
            == max_date - pd.Timedelta(days=window_size - col - 1)
        ]
        item_stats = item_stats.join(
            watches.groupby("item_id")["user_id"].count(), lsuffix=col
        )
    item_stats = item_stats.fillna(0)

    item_stats["trend_slope"] = item_stats.apply(
        trend_slope, axis=1, window_size=window_size
    )

    # Find total views for time_window period
    new_colnames = ["user_id" + str(i) for i in range(1, window_size)] + ["user_id"]
    item_stats["watched_in_7_days"] = item_stats[new_colnames].apply(sum, axis=1)

    # View statistics
    interactions_df.loc[:, "day_of_year"] = interactions_df["datetime"].dt.dayofyear

    watches = interactions_df.groupby("item_id").agg(
        {"day_of_year": [lambda x: x.quantile(0.95), lambda x: x.quantile(0.5), np.std]}
    )
    watches.columns = watches.columns.droplevel()
    watches.columns = ["watch_ts_quantile_95", "watch_ts_median", "watch_ts_std"]
    max_date_dayofyear = max_date.timetuple().tm_yday
    watches["watch_ts_quantile_95_diff"] = (
        max_date_dayofyear - watches["watch_ts_quantile_95"]
    )
    watches["watch_ts_median_diff"] = max_date_dayofyear - watches["watch_ts_median"]
    watches = watches.reset_index()

    # Total views
    watched_all_time = interactions_df.groupby("item_id")["user_id"].count()
    watched_all_time.name = "watched_in_all_time"
    watches = watches.join(watched_all_time, on="item_id", how="left")
    watches["not_outsider"] = (watches["watched_in_all_time"] > 10).astype("int32")
    watches["not_low_watch_cnt"] = (watches["watched_in_all_time"] > 30).astype("int32")

    # Share of watches bins
    watches = watches.sort_values(by="watched_in_all_time")
    watches_sum = watches["watched_in_all_time"].sum()
    share_of_watches = watches["watched_in_all_time"] / watches_sum
    cum_share_of_watches = share_of_watches.cumsum()
    watches["share_of_watches_bin_10"] = pd.cut(cum_share_of_watches, 10, labels=False)
    watches["share_of_watches_bin_5"] = pd.cut(cum_share_of_watches, 5, labels=False)
    watches["share_of_watches_bin_3"] = pd.cut(cum_share_of_watches, 3, labels=False)

    # Ids for top items
    watches = watches.sort_values(by="watched_in_all_time", ascending=False)
    watches["id_for_top"] = -1
    watches.loc[:10, "id_for_top"] = watches.loc[:10, "item_id"]

    # Finalize feature output
    item_stats = item_stats.join(watches.set_index("item_id"), on="item_id", how="left")
    item_stats = item_stats.fillna(0)
    item_stats = item_stats.reset_index()

    item_stats["watched_in_7_days"] = item_stats["watched_in_7_days"].astype(int)
    item_stats["watched_in_all_time"] = item_stats["watched_in_all_time"].astype(int)

    added_cols = [
        "item_id",
        "trend_slope",
        "watched_in_7_days",
        "watch_ts_quantile_95_diff",
        "watch_ts_median_diff",
        "watch_ts_std",
        "watched_in_all_time",
        "share_of_watches_bin_10",
        "share_of_watches_bin_5",
        "share_of_watches_bin_3",
        "not_outsider",
        "not_low_watch_cnt",
        "id_for_top",
    ]
    return item_stats[added_cols]


def add_age_stats(
    interactions_df: pd.DataFrame,
    items_df: pd.DataFrame,
    users_df: pd.DataFrame,
    max_date: Union[str, datetime] = "2021-08-22",
) -> pd.DataFrame:
    """
    Computes watchers age stats for items with particular interactions date split and
        adds them to item_stats dataframe.

    Args:
    - interactions_df (pd.DataFrame): pd.DataFrame with interactiond data
        columns: ["user_id", "item_id", "datetime"].
    - items_df (pd.DataFrame): pd.DataFrame with column "item_id".
    - users_df (pd.DataFrame): pd.DataFrame with columns ["user_id", "sex", "age", "income"].
    - max_date (datetime or str): The maximum date for the interactions.

    Returns:
    - DataFrame: item_stats with added columns "younger_35_fraction" and "older_35_fraction",
        representing the fraction of watchers that are younger than 35 and
        older than or equal to 35.
    """
    if isinstance(max_date, str):
        max_date = datetime.strptime(max_date, "%Y-%m-%d")

    # Limit interaction history to prevent data leak
    interactions_df = interactions_df[interactions_df["datetime"] <= max_date].copy()

    # Calculate features
    interactions_df = interactions_df.merge(
        users_df[["user_id", "sex", "age", "income"]], on="user_id", how="left"
    )

    interactions_df["age_overall"] = interactions_df["age"].replace(
        to_replace={
            "age_18_24": "less_35",
            "age_25_34": "less_35",
            "age_35_44": "over_35",
            "age_45_54": "over_35",
            "age_55_64": "over_35",
            "age_65_inf": "over_35",
        }
    )
    age_stats = (
        interactions_df.groupby("item_id")["age_overall"]
        .value_counts(normalize=True)
        .rename("value")
        .reset_index()
    )

    age_stats = age_stats.pivot(
        index="item_id", columns="age_overall", values="value"
    ).drop("age_unknown", axis=1)

    # Allign features by item_id
    item_stats = items_df[["item_id"]]
    item_stats = item_stats.join(age_stats, on="item_id")
    item_stats = item_stats.fillna(0)
    item_stats = item_stats.rename(
        columns={"less_35": "younger_35_fraction", "over_35": "older_35_fraction"}
    )
    item_stats["youth_bin"] = pd.cut(item_stats["younger_35_fraction"], 3, labels=False)
    return item_stats


def add_sex_stats(
    interactions_df: pd.DataFrame,
    items_df: pd.DataFrame,
    users_df: pd.DataFrame,
    max_date: Union[str, datetime] = "2021-08-22",
) -> pd.DataFrame:
    """
    Computes watchers sex stats for items with particular interactions date split and
        adds them to item_stats dataframe.

    Args:
    - interactions_df (pd.DataFrame): pd.DataFrame with interactiond data
        columns: ["user_id", "item_id", "datetime"].
    - items_df (pd.DataFrame): pd.DataFrame with column "item_id".
    - users_df (pd.DataFrame): pd.DataFrame with columns ["user_id", "sex", "age", "income"].
    - max_date (datetime or str): The maximum date for the interactions.

    Returns:
    - DataFrame: item_stats with item_id as the index and columns for male_watchers_fraction.
        and female_watchers_fraction.
    """

    if isinstance(max_date, str):
        max_date = datetime.strptime(max_date, "%Y-%m-%d")

    # Limit interaction history to prevent data leak
    interactions_df = interactions_df[interactions_df["datetime"] <= max_date].copy()

    # Calculate features
    interactions_df = interactions_df.merge(
        users_df[["user_id", "sex", "age", "income"]], on="user_id", how="left"
    )

    sex_stats = (
        interactions_df.groupby("item_id")["sex"]
        .value_counts(normalize=True)
        .rename("value")
        .reset_index()
    )

    sex_stats = sex_stats.pivot(index="item_id", columns="sex", values="value").drop(
        "sex_unknown", axis=1
    )

    # Allign features by item_id
    item_stats = items_df[["item_id"]]
    item_stats = item_stats.join(sex_stats, on="item_id")
    item_stats = item_stats.fillna(0)
    item_stats = item_stats.rename(
        columns={"F": "female_watchers_fraction", "M": "male_watchers_fraction"},
    )
    item_stats["brutality_bin"] = pd.cut(
        item_stats["male_watchers_fraction"], 3, labels=False
    )
    return item_stats


@click.command()
@click.argument("interactions_input_path", type=click.Path())
@click.argument("items_input_path", type=click.Path())
@click.argument("users_input_path", type=click.Path())
@click.argument("items_output_path", type=click.Path())
@click.option("--date", type=str)
@click.option("--window", type=int)
def add_item_stats(
    interactions_input_path: str,
    items_input_path: str,
    users_input_path: str,
    items_output_path: str,
    date: str,
    window: int,
) -> None:
    """
    Calculate timde-depends items features and save the result to a CSV file.

    Args:
    - interactions_input_path (str): Path to the CSV file with the user-item interactions.
    - items_input_path (str): Path to the CSV file with the item data.
    - users_input_path (str): Path to the CSV file with the user data.
    - items_output_path (str): Path to save the CSV file with the item features.
    - date (str): Date string (YYYY-MM-DD format) to define the end of the time.
        window for calculating time-based features.
    - window (int): Number of days to define the time window for calculating time-based features.

    Returns:
    - None.
    """
    logging.basicConfig(level=logging.INFO)
    logging.info(f"Adding item time-based features for {date} and window {window}")

    # Read data
    interactions_df = pd.read_csv(interactions_input_path, parse_dates=["datetime"])
    items_df = pd.read_csv(items_input_path)
    users_df = pd.read_csv(users_input_path)

    # Compute features and save data to date folder
    item_stats = add_item_watches_stats(interactions_df, items_df, date, window)
    age_stats = add_age_stats(interactions_df, items_df, users_df, date)
    sex_stats = add_sex_stats(interactions_df, items_df, users_df, date)

    result = item_stats.merge(age_stats, on="item_id", how="left")
    result = result.merge(sex_stats, on="item_id", how="left")
    result = result.fillna(0)
    result.to_csv(items_output_path, index=False)


if __name__ == "__main__":
    add_item_stats()


# Force to run this part
# snakemake -c1 -r add_item_stats_time_based
