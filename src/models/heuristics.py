# pylint: disable=abstract-method

import typing as tp
from datetime import datetime
from enum import Enum

import numpy as np
import pandas as pd
from pandas import DataFrame
from rectools import Columns, ExternalIds
from rectools.dataset import Dataset
from rectools.models.base import ModelBase

from src.models.tools import (
    ModelDescription,
    get_recommender_from_settings,
    reduce_weights,
)


class Heuristic(str, Enum):
    INTERACTIONS_DAYS_OFFSET = "INTERACTIONS_DAYS_OFFSET"
    INTERACTIONS_LIMIT = "INTERACTIONS_LIMIT"
    SKIP_UNPOPULAR = "SKIP_UNPOPULAR"
    RANK_ON_COSINE = "RANK_ON_COSINE"


class CompleteModel(ModelBase):
    def __init__(
        self, model: ModelBase, heurisitcs: tp.Dict[Heuristic, tp.Any], verbose: int = 0
    ) -> None:
        super().__init__(verbose=verbose)
        self.model = model
        self.heuristics = {
            key: value for key, value in heurisitcs.items() if value is not None
        }

    def _fit(self, dataset: Dataset, *args, **kwargs):
        if (
            Heuristic.INTERACTIONS_DAYS_OFFSET in self.heuristics
            and self.heuristics[Heuristic.INTERACTIONS_DAYS_OFFSET] is not None
        ):
            day_offset = self.heuristics[Heuristic.INTERACTIONS_DAYS_OFFSET]
            dataset = reduce_weights(dataset, day_offset=day_offset)
        if (
            Heuristic.INTERACTIONS_LIMIT in self.heuristics
            and self.heuristics[Heuristic.INTERACTIONS_LIMIT] is not None
        ):
            interaction_offset = self.heuristics[Heuristic.INTERACTIONS_LIMIT]
            dataset = reduce_weights(dataset, interaction_offset=interaction_offset)

        self.model.fit(dataset, *args, **kwargs)

    def recommend(
        self,
        users: ExternalIds,
        dataset: Dataset,
        k: int,
        filter_viewed: bool,
        items_to_recommend: tp.Optional[ExternalIds] = None,
        add_rank_col: bool = True,
    ) -> DataFrame:
        if (
            Heuristic.SKIP_UNPOPULAR in self.heuristics
            and self.heuristics[Heuristic.SKIP_UNPOPULAR] is not None
        ):
            int_count = dataset.interactions.df.groupby(Columns.Item)[
                Columns.User
            ].count()
            border = self.heuristics[Heuristic.SKIP_UNPOPULAR]
            good_items = int_count[int_count > border].index
            good_items_ext = dataset.item_id_map.convert_to_external(good_items)
            if items_to_recommend is None:
                items_to_recommend = good_items_ext
            else:
                items_to_recommend = np.intersect1d(good_items_ext, items_to_recommend)
        if (
            Heuristic.RANK_ON_COSINE in self.heuristics
            and self.heuristics[Heuristic.RANK_ON_COSINE] is not None
        ):
            num_dummies = self.heuristics[Heuristic.RANK_ON_COSINE]
            recos = self.model.recommend(
                dataset=dataset,
                users=users,
                items_to_recommend=items_to_recommend,
                k=k * 2,
                filter_viewed=filter_viewed,
                add_rank_col=False,
            )
            on_date = datetime.strftime(
                dataset.interactions.df.datetime.max(), "%Y-%m-%d"
            )
            add_genres_dummy_cosine(recos, on_date=on_date, num_dummies=num_dummies)
            recos = rank_on_cosine(recos, k, cosine_rank_weight=2)
            return recos

        recos = self.model.recommend(
            dataset=dataset,
            users=users,
            items_to_recommend=items_to_recommend,
            k=k,
            filter_viewed=filter_viewed,
            add_rank_col=add_rank_col,
        )
        return recos


def get_complete_model_from_settings(
    model_settings: ModelDescription,
    heuristics: tp.Optional[tp.Dict[Heuristic, tp.Dict[str, tp.Any]]] = None,
) -> ModelBase:
    reommender = get_recommender_from_settings(model_settings)
    if heuristics is None:
        return reommender
    return CompleteModel(reommender, heurisitcs=heuristics)


def rank_on_cosine(
    recos_with_cosine: pd.DataFrame, num_recos: int, cosine_rank_weight: float = 2.0
) -> pd.DataFrame:

    recos = recos_with_cosine.sort_values(
        by=["genres_dummy_cosine", "score"], ascending=False
    )
    if "rank" not in recos.columns:
        recos["rank"] = recos.groupby("user_id").cumcount() + 1
    recos = recos.rename(columns={"rank": "first_stage_rank"})
    recos["cosine_rank"] = recos.groupby("user_id").cumcount() + 1
    recos["mid_rank"] = (
        cosine_rank_weight * recos["cosine_rank"] + recos["first_stage_rank"]
    )
    recos = (
        recos.sort_values(by=["mid_rank", "cosine_rank"])
        .groupby("user_id")
        .head(num_recos)
        .drop(
            columns=[
                "mid_rank",
                "cosine_rank",
                "first_stage_rank",
                "genres_dummy_cosine",
            ]
        )
    )
    recos["rank"] = recos.groupby("user_id").cumcount() + 1
    recos["score"] = recos["rank"]
    return recos


def add_genres_dummy_cosine(
    first_stage_candidates: pd.DataFrame,
    on_date: str,
    num_dummies: tp.Optional[int] = None,
) -> pd.DataFrame:
    items_static = pd.read_csv("data/processed/items_processed_static.csv")
    users_time_based = pd.read_csv(f"data/processed/{on_date}/user_features.csv")
    candidates_with_dummy = first_stage_candidates[["user_id", "item_id"]].copy()
    dummy_country_cols = [
        col_name
        for col_name in items_static.columns
        if col_name.startswith("country_dummy_")
    ]
    dummy_genre_cols = [
        col_name
        for col_name in items_static.columns
        if col_name.startswith("genre_dummy_")
    ]
    dummy_feature_cols = dummy_country_cols + dummy_genre_cols

    if num_dummies is not None:
        dummy_feature_cols = dummy_feature_cols[:num_dummies]

    candidates_with_dummy = candidates_with_dummy.merge(
        items_static[["item_id"] + dummy_feature_cols], on="item_id", how="left"
    )
    users_with_dummy = first_stage_candidates[["user_id"]].copy().drop_duplicates()
    users_with_dummy = users_with_dummy.merge(
        users_time_based[["user_id"] + dummy_feature_cols], on="user_id", how="left"
    )

    grouped = candidates_with_dummy.groupby("user_id")
    result_df_list = []
    for user_id, group_df in grouped:
        user_vector = users_with_dummy[users_with_dummy["user_id"] == user_id]
        group_df["genres_dummy_dot"] = (
            group_df[dummy_feature_cols] @ user_vector[dummy_feature_cols].T
        )
        group_df["genres_dummy_norms"] = np.linalg.norm(
            group_df[dummy_feature_cols].values, axis=1
        )
        group_df["genres_dummy_cosine"] = (
            group_df["genres_dummy_dot"] / group_df["genres_dummy_norms"]
        ) / np.linalg.norm(user_vector[dummy_feature_cols].values)
        result_df_list.append(group_df)
    candidates_result = pd.concat(result_df_list)
    first_stage_candidates["genres_dummy_cosine"] = candidates_result[
        "genres_dummy_cosine"
    ]
