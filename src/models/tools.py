# pylint: disable=import-error, wrong-import-position
"""Useful tools for recommendations"""

import typing as tp
from copy import deepcopy
from datetime import datetime, timedelta
from enum import Enum

import pandas as pd
import yaml
from implicit.als import AlternatingLeastSquares
from implicit.nearest_neighbours import (
    BM25Recommender,
    CosineRecommender,
    TFIDFRecommender,
)
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from rectools import Columns
from rectools.dataset import Dataset, Interactions
from rectools.models import (
    ImplicitALSWrapperModel,
    ImplicitItemKNNWrapperModel,
    PopularInCategoryModel,
    PopularModel,
    RandomModel,
)
from rectools.models.base import ModelBase


class ModelType(str, Enum):
    POPULAR = "popular"
    CAT_POPULAR = "cat_popular"
    TFIDF = "tf_idf"
    BM25 = "bm25"
    BOOSTING = "boosting"
    COSINE = "cosine"
    RANDOM = "random"
    ALS = "als"


class ModelDescription(BaseModel):
    model_type: str
    params: tp.Union[tp.Dict[str, tp.Any], None]


def parse_pipeline_config(pipeline_config_path: str) -> tp.Dict[str, tp.Any]:
    with open(pipeline_config_path, "r", encoding="utf-8") as yamlfile:
        pipeline_config = yaml.load(yamlfile, Loader=yaml.FullLoader)
    dates = pipeline_config["pipe_dates"]
    return dates


def get_recommender_from_settings(model_setting: ModelDescription) -> ModelBase:
    """Getting Rectools Recommender model from config

    Parameters
    ----------
    model_setting : ModelDescription
        Model config for Rectools Recommender model

    Returns
    -------
    ModelBase
        Rectools Recommender model

    Raises
    ------
    ValueError
        Raises when model type is not known
    """
    model_type = model_setting.model_type
    model_params = model_setting.params

    if model_params is None:
        model_params = {}

    if "days" in model_params:
        final_params = {
            key: value for key, value in model_params.items() if key != "days"
        }
        final_params["period"] = timedelta(days=model_params["days"])
    else:
        final_params = model_params

    if model_type == ModelType.POPULAR:
        recommender = PopularModel(**final_params)
    elif model_type == ModelType.CAT_POPULAR:
        recommender = PopularInCategoryModel(**final_params)
    elif model_type == ModelType.TFIDF:
        recommender = ImplicitItemKNNWrapperModel(
            model=TFIDFRecommender(**final_params)
        )
    elif model_type == ModelType.COSINE:
        recommender = ImplicitItemKNNWrapperModel(
            model=CosineRecommender(**final_params)
        )
    elif model_type == ModelType.BM25:
        recommender = ImplicitItemKNNWrapperModel(model=BM25Recommender(**final_params))
    elif model_type == ModelType.RANDOM:
        recommender = RandomModel(**final_params)
    elif model_type == ModelType.ALS:
        recommender = ImplicitALSWrapperModel(
            model=AlternatingLeastSquares(**final_params), verbose=100
        )
    else:
        raise ValueError(f"Recommender model type '{model_type}' is not known")
    return recommender


def get_model_name_from_settings(model_setting: ModelDescription) -> str:
    model_type = model_setting.model_type
    model_params = model_setting.params
    if model_params is None:
        return model_type
    return model_type + "_".join(
        [str(key[:5]) + str(value) for key, value in model_params.items()]
    )


def check_na_values_in_features(data: pd.DataFrame) -> None:
    """Check if any na values are present in Dataframe

    Parameters
    ----------
    data : pd.DataFrame
        Dataframe with columns `Columns.User`, `Columns.Item` and feature columns

    Raises
    ------
    ValueError
        Raised if any of the feature columns have na values
    """
    feature_columns = data.columns.drop(Columns.UserItem)
    for col_name in feature_columns:
        num_na = data[col_name].isna().sum()
        if num_na > 0:
            raise ValueError(
                f"{col_name} feature is not present for all items. Found {num_na} na values"
            )


def add_fallback_reco(
    reco_main: pd.DataFrame,
    reco_fallback: pd.DataFrame,
    k: int,
    add_rank_col: bool = True,
) -> pd.DataFrame:
    """Fulfills main recos with fallback recos if necessary. Fallback recos are added for a user
    when he doesn't have enough number of items in main recos.

    Parameters
    ----------
    reco_main : pd.DataFrame
        Main recommendations
    reco_fallback : pd.DataFrame
        Fallback recommendations, usually from Popular algorithm
    k : int
        Target number of recos for each user
    add_rank_col : bool, optional
        Adding columns with reco ranks, by default True

    Returns
    -------
    pd.DataFrame
        Final recommendations where each user has exactly `k` recos. Main recos have higher ranks
        when fallback recos are added for a user
    """

    reco_main["rank"] = reco_main.groupby(Columns.User, sort=False).cumcount()
    reco_fallback["rank"] = (
        reco_fallback.groupby(Columns.User, sort=False).cumcount()
        + reco_main["rank"].max()
        + 1
    )
    full_recos = (
        pd.concat((reco_main, reco_fallback), sort=False)
        .sort_values([Columns.User, "rank"])
        .drop_duplicates(subset=Columns.UserItem, keep="first")
        .groupby(Columns.User, sort=False)
        .head(k)
    )
    reco_main.drop(columns=["rank"], inplace=True)
    reco_fallback.drop(columns=["rank"], inplace=True)
    full_recos.drop(columns=["score", "rank"], inplace=True)

    if add_rank_col:
        full_recos[Columns.Rank] = (
            full_recos.groupby(Columns.User, sort=False).cumcount() + 1
        )
        full_recos[Columns.Rank] = full_recos[Columns.Rank].astype("int32")

    full_recos[Columns.User] = full_recos[Columns.User].astype("int64")
    full_recos[Columns.Item] = full_recos[Columns.Item].astype("int64")

    return full_recos


def convert_categorical_features_df_to_rectools_format(
    features_df: pd.DataFrame, id_col: str
) -> pd.DataFrame:
    """Transforms features Dataframe to format which is acceptable by Rectools library to construct
    Dataset

    Parameters
    ----------
    features_df : pd.DataFrame
        Data table with `id_col` and feature columns where each column represents one feature
    id_col : str
        Name of id column

    Returns
    -------
    pd.DataFrame
        Data table with columns `id_col`, `feature` and `value`
    """
    result_dfs = []
    for col_name in features_df.columns.drop(id_col):
        category_df = features_df[[id_col, col_name]].rename(
            columns={col_name: "value", id_col: "id"}
        )
        category_df["feature"] = col_name
        result_dfs.append(category_df)
    return pd.concat(result_dfs, axis=0, sort=False)


def convert_category_string_features_to_rectools_format(
    categories_df: pd.DataFrame, id_col: str
) -> pd.DataFrame:
    """Transforms string categorical features Dataframe to format which is acceptable by Rectools
    library to construct Dataset

    Parameters
    ----------
    categories_df : pd.DataFrame
        Data table with `id_col` and feature columns where each column represents one feature and
        categories are presented in string format with `, ` separator for listing categories
    id_col : str
        Name of id column

    Returns
    -------
    pd.DataFrame
        Data table with columns `id_col`, `feature` and `value`
    """
    result_dfs = []
    for col_name in categories_df.columns.drop(id_col):
        category_df = pd.DataFrame(
            {
                "id": categories_df[id_col],
                "value": categories_df[col_name].str.split(", "),
            }
        )
        category_df = category_df.explode("value")
        category_df["feature"] = col_name
        result_dfs.append(category_df)
    return pd.concat(result_dfs, axis=0, sort=False)


def replace_interactions_in_dataset(
    dataset: Dataset, new_interactions: pd.DataFrame
) -> Dataset:
    """Create new Dataset where interactions are replaced with provided new interactions

    Parameters
    ----------
    dataset : Dataset
        Rectools dataset
    new_interactions : pd.DataFrame
        New interactions for Dataset. Users and items ids mapping must be consistent with the
        Dataset

    Returns
    -------
    Dataset
        Dataset with replaced interactions
    """
    replacing_interactions = Interactions(new_interactions)
    return Dataset(
        user_id_map=dataset.user_id_map,
        item_id_map=dataset.item_id_map,
        interactions=replacing_interactions,
        user_features=dataset.user_features,
        item_features=dataset.item_features,
    )


def reduce_weights(
    dataset: Dataset,
    day_offset: tp.Union[int, None] = None,
    interaction_offset: tp.Union[int, None] = None,
    new_weight: float = 1e-6,
    before_date: tp.Union[datetime, None] = None,
):
    """Reduce the weights of interactions in the dataset based on
    specified offsets and a new weight. There are two types of offsets:
    * By interaction age (in days)
    * By the number of interactions after

    Parameters
    ----------
    dataset : Dataset
        Rectools Dataset.
    day_offset : int, optional
        The number of days to consider for reducing weights based on interaction age.
        If None, no penalty based on interaction age is applied. Default is None.
    interaction_offset : int, optional
        The number of interactions to consider for reducing weights based on interaction count.
        If None, no penalty based on interaction count is applied. Default is None.
    new_weight : float
        The new weight value to assign to interactions that meet the specified offsets.
        Default is 1e-6.

    Returns
    -------
    Dataset
        Dataset with replaced weights
    """
    if before_date is not None:
        inter_df = deepcopy(
            dataset.interactions.df[
                dataset.interactions.df[Columns.Datetime] < before_date
            ]
        )
        unchanged_df = deepcopy(
            dataset.interactions.df[
                dataset.interactions.df[Columns.Datetime] >= before_date
            ]
        )
    else:
        inter_df = deepcopy(dataset.interactions.df)

    # set penalty by interactions amount
    if interaction_offset is not None:
        inter_df["interaction_count"] = (
            inter_df.sort_values(by=["datetime"], ascending=False)
            .groupby("user_id")
            .cumcount()
            + 1
        )
        inter_df.loc[
            inter_df["interaction_count"] > interaction_offset, "weight"
        ] = new_weight
        inter_df = inter_df.drop(columns=["interaction_count"])

    # set penalty by iteraction age
    if day_offset is not None:
        min_date = inter_df["datetime"].max() - pd.DateOffset(days=day_offset)
        inter_df.loc[inter_df["datetime"] < min_date, "weight"] = new_weight

    if before_date is not None:
        inter_df = pd.concat([inter_df, unchanged_df], axis=0)

    return Dataset(
        user_id_map=dataset.user_id_map,
        item_id_map=dataset.item_id_map,
        interactions=Interactions(inter_df),
        user_features=dataset.user_features,
        item_features=dataset.item_features,
    )
