# pylint: disable=wrong-import-position, import-error, abstract-method
"""2-stage Recommender Model very close to RecTools API"""

import logging
import typing as tp
from datetime import datetime
from enum import Enum
from functools import reduce

import numpy as np
import pandas as pd
from catboost import CatBoostRanker, Pool
from numpy.random import default_rng
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from pydantic import root_validator
from rectools import Columns, ExternalIds
from rectools.dataset import Dataset
from rectools.models.base import ModelBase

from src.models.tools import (
    ModelDescription,
    get_recommender_from_settings,
    replace_interactions_in_dataset,
)
from src.models.two_stage_blocks import add_genres_dummy_cosine


def calc_features_example(
    first_stage_candidates: pd.DataFrame,
    dataset: Dataset,  # pylint: disable=unused-argument
) -> pd.DataFrame:
    """Default calculating features func. Keeping it means using inly ranks and scores from
    first-stage models without adding any features.

    Parameters
    ----------
    first_stage_candidates : pd.DataFrame
        Data table with with columns `Columns.User`, `Columns.Item`, `target` and optional score and
        rank columns from first-stage models
    dataset : Dataset, optional
        Rectools Dataset which may be used to calculate features if needed

    Returns
    -------
    pd.DataFrame
        Data table with with columns `Columns.User`, `Columns.Item`, `target`, optional score and
        rank columns from first-stage models and computed features for candidates
    """
    return first_stage_candidates


def training_example(train_with_target: pd.DataFrame) -> CatBoostRanker:
    """Default training func for second stage

    Parameters
    ----------
    train_with_target : pd.DataFrame
        Data table with with columns `Columns.User`, `Columns.Item`, `target`, optional score and
        rank columns from first-stage models and computed features for candidates

    Returns
    -------
    CatBoostRanker
        Fitted second stage model for recommendations
    """

    model_params = {
        "n_estimators": 1000,
        "loss_function": "YetiRank",
    }

    # Prepare catboost pool
    train = train_with_target.sort_values(by=Columns.User)
    x_train = train.drop([Columns.User, Columns.Item, "target"], axis=1)
    y_train = train["target"].values
    group_id = train[Columns.User].values
    train_pool = Pool(
        data=x_train,
        label=y_train,
        group_id=group_id,
        cat_features=None,
    )

    second_stage_model = CatBoostRanker(verbose=0, **model_params)
    second_stage_model.fit(train_pool)
    return second_stage_model


class FirstStageModel(BaseModel):
    """
    Pydantic dataclass for a first stage model config with explicit validation for provided values
    """

    model_description: ModelDescription
    num_candidates: int
    keep_scores: bool
    keep_ranks: bool
    scores_fillna_value: tp.Optional[float] = None
    ranks_fillna_value: tp.Optional[float] = None

    @root_validator
    def kept_scores_have_fillna_value(  # pylint: disable=no-self-argument
        cls: "FirstStageModel", values: tp.Dict[str, tp.Any]  # noqa: N805
    ) -> tp.Dict[str, tp.Any]:
        keep_scores = values.get("keep_scores")
        scores_fillna_value = values.get("scores_fillna_value")
        if keep_scores and scores_fillna_value is None:
            raise ValueError(
                "First-stage model needs scores_fillna_value with keep_scores=True"
            )
        return values

    @root_validator
    def kept_ranks_have_fillna_value(  # pylint: disable=no-self-argument
        cls: "FirstStageModel", values: tp.Dict[str, tp.Any]  # noqa: N805
    ) -> tp.Dict[str, tp.Any]:
        keep_ranks = values.get("keep_ranks")
        ranks_fillna_value = values.get("ranks_fillna_value")
        if keep_ranks and ranks_fillna_value is None:
            raise ValueError(
                "First-stage model needs ranks_fillna_value with keep_ranks=True"
            )
        return values


FirstStageConfig = tp.Dict[str, FirstStageModel]


class SamplingStrategy(str, Enum):
    PER_POSITIVE = "per_positive"
    PER_USER = "per_user"
    PER_POSITIVE_POP_BALANCED = "per_positive_pop_balanced"
    PER_USER_COSINE_HARD = "per_user_cosine_hard"


class SamplingConfig(BaseModel):
    num_neg_samples: int = 3
    train_period_n_days: int = 7
    sampling_strategy: SamplingStrategy = SamplingStrategy.PER_POSITIVE
    random_state: tp.Optional[int] = None


class TwoStageRecommender(ModelBase):
    """
    This is a Recommender model class for easy use of two-stage approach for building
    recommendations with Rectools library. It supports generating candidates from multiple
    first-stage models with optionally keeping ranks and/or scores. Custom feature calculating func
    and custom training func are meant to be used in the pipeline for training/inference.
    Parameters
    ----------
    first_stage_models_config : FirstStageConfig
        Config for all first-stage models used for candidates generation
    sampling_config : tp.Optional[SamplingConfig], optional
        Training parameters for second stage model
    verbose : int, optional
        Verbose, by default 0
    """

    def __init__(
        self,
        first_stage_models_config: FirstStageConfig,
        sampling_config: tp.Optional[SamplingConfig] = None,
        verbose: int = 0,
    ) -> None:

        super().__init__(verbose=verbose)
        self.first_stage_models_config = first_stage_models_config
        self.sampling_config = sampling_config
        if sampling_config is not None:
            sampling_cfg_parsed = SamplingConfig.parse_obj(sampling_config)
            self.train_period_n_days = sampling_cfg_parsed.train_period_n_days
            self.num_neg_samples = sampling_cfg_parsed.num_neg_samples
            self.sampling_strategy = sampling_cfg_parsed.sampling_strategy
            self.random_state = sampling_cfg_parsed.random_state
        self.recommenders: tp.Dict[str, ModelBase]
        self.second_stage_model: tp.Any
        self.input_example: pd.DataFrame

    def _get_fitted_first_stage_recommenders(
        self, dataset: Dataset
    ) -> tp.Dict[str, ModelBase]:

        recommenders = {}
        for model_name, model_config in self.first_stage_models_config.items():
            model_settings = model_config.model_description
            recommenders[model_name] = get_recommender_from_settings(model_settings)
            recommenders[model_name].fit(dataset)
        return recommenders

    def _fit(self, dataset: Dataset, *args: tp.Any, **kwargs: tp.Any) -> None:
        """
        This method overwrites RecTools _fit. Fits all first-stage models on the dataset
        """
        self.recommenders = self._get_fitted_first_stage_recommenders(dataset)

    def _split_to_history_and_train_interactions(
        self, interactions: pd.DataFrame
    ) -> tp.Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Splitting interactions to history and train. History interactions provide data to get
        candidates from first-stage models. Train interactions provide data for second stage model
        training. Train interactions keep only users and items that were present in history
        interactions.
        """
        last_date = interactions[Columns.Datetime].max()
        logger = logging.getLogger("TWO_STAGE_MODEL")
        logger.info(f"Interactions last date: {last_date.date()}")
        train_start_date = last_date - pd.Timedelta(days=self.train_period_n_days - 1)

        history_interactions = interactions[
            interactions[Columns.Datetime] < train_start_date
        ]
        train_interactions = interactions[
            interactions[Columns.Datetime] >= train_start_date
        ]
        logger.info(
            f"Train interactions start date: {train_interactions['datetime'].min().date()}"
        )

        # Filter new users from train_interactions
        history_users = history_interactions[Columns.User].drop_duplicates()
        train_interactions = pd.merge(
            train_interactions, history_users, how="inner", on=Columns.User
        )

        # Filter new items from train_interactions
        history_items = history_interactions[Columns.Item].drop_duplicates()
        train_interactions = pd.merge(
            train_interactions, history_items, how="inner", on=Columns.Item
        )
        return history_interactions, train_interactions

    def recommend(
        self,
        users: ExternalIds,
        dataset: Dataset,
        k: int,
        filter_viewed: bool,
        items_to_recommend: tp.Optional[ExternalIds] = None,
        add_rank_col: bool = True,
        calc_features_func: tp.Callable = calc_features_example,
        calc_features_func_kwargs: tp.Optional[tp.Dict[str, tp.Any]] = None,
    ) -> pd.DataFrame:
        """
        Recommend items for users.
        To use this method model must be fitted.
        Parameters
        ----------
        users : np.ndarray
            Array of external user ids to recommend for.
        dataset : Dataset
            Dataset with input data.
            Usually it's the same dataset that was used to fit model.
        k : int
            Derived number of recommendations for every user.
            Pay attention that in some cases real number of recommendations may be less than `k`.
        filter_viewed : bool
            Whether to filter from recommendations items that user has already interacted with.
        items_to_recommend : np.ndarray, optional
            Whitelist of item external ids.
            If given, only these items will be used for recommendations.
            Otherwise all items from dataset will be used.
        add_rank_col : bool, default True
            Whether to add rank column to recommendations.
            If True column `Columns.Rank` will be added.
            This column contain integers from 1 to ``number of user recommendations``.
            In any case recommendations are sorted per rank for every user.
            The lesser the rank the more recommendation is relevant.
        calc_features_func: tp.Callable, default calc_features_example
            Function that receives candidates dataframe, Dataset and optional kwargs and calculates
            all necessary features for second stage
        calc_features_func_kwargs: tp.Optional[tp.Dict[str, tp.Any]]
            Optional kwargs to pass for custom calc_features_func
        Returns
        -------
        pd.DataFrame
            Recommendations table with columns `Columns.User`, `Columns.Item`, `Columns.Score`,
            `Columns.Rank`].
            1st column contains external user ids,
            2nd - external ids of recommended items sorted for each user by relevance,
            3rd - score that model gives for the user-item pair,
            4th (present only if `add_rank_col` is ``True``) - integers from ``1`` to number of user
            recommendations.
            Recommendations for every user are always sorted by relevance.

        """
        # First stage
        first_stage_candidates = self._get_candidates_from_first_stage(
            self.recommenders, users, dataset, filter_viewed, items_to_recommend
        )
        if calc_features_func_kwargs is None:
            calc_features_func_kwargs = {}
        candidates_with_features = calc_features_func(
            first_stage_candidates,
            dataset,
            for_inference=True,
            **calc_features_func_kwargs,
        )

        # Second stage
        second_stage_recos = candidates_with_features[Columns.UserItem].copy()
        x_full = candidates_with_features.drop(columns=Columns.UserItem)
        x_full = x_full[self.second_stage_model.feature_names_]
        second_stage_recos[Columns.Score] = self.second_stage_model.predict_proba(
            x_full
        )[:, 1]
        second_stage_recos.sort_values(
            by=[Columns.User, Columns.Score], ascending=False, inplace=True
        )
        second_stage_recos = second_stage_recos.groupby(Columns.User).head(k)
        if add_rank_col:
            second_stage_recos[Columns.Rank] = (
                second_stage_recos.groupby(Columns.User, sort=False).cumcount() + 1
            )

        return second_stage_recos

    def _set_targets_and_sample_negatives(
        self,
        candidates: pd.DataFrame,
        train_interactions: pd.DataFrame,
    ) -> pd.DataFrame:
        """Prepare train with target out of candidates and interactions

        Parameters
        ----------
        candidates : pd.DataFrame
            Data table with first stage candidates and features
        train_interactions : pd.DataFrame
            Interactions during train period, considered positive targets

        Returns
        -------
        pd.DataFrame
            Train with features and target data table

        Raises
        ------
        ValueError
            When sampling_strategy is not known
        """

        # Set targets
        train_interactions["target"] = 1

        # Remember that this way we exclude positives that weren't present in candidates
        train = pd.merge(
            candidates,
            train_interactions[[Columns.User, Columns.Item, "target"]],
            how="left",
            on=Columns.UserItem,
        )
        train["target"] = train["target"].fillna(0)
        train["target"] = train["target"].astype("int32")
        pos = train[train["target"] == 1]

        if self.sampling_strategy == SamplingStrategy.PER_USER:
            # Some users might not have enough negatives for sampling
            num_negatives = (
                train[train["target"] == 0]
                .groupby([Columns.User])[Columns.Item]
                .count()
            )
            sampling_mask = train[Columns.User].isin(
                num_negatives[num_negatives > self.num_neg_samples].index
            )
            negative_mask = train["target"] == 0
            neg_for_sample = train[sampling_mask & negative_mask]
            neg = neg_for_sample.groupby([Columns.User], sort=False).apply(
                pd.DataFrame.sample,
                n=self.num_neg_samples,
                replace=False,
                random_state=self.random_state,
            )
            neg = pd.concat([neg, train[(~sampling_mask) & negative_mask]], axis=0)
        elif self.sampling_strategy == SamplingStrategy.PER_USER_COSINE_HARD:
            # train["id"] = train.index
            # num_positives = pos.groupby("user_id")["item_id"].count()
            # num_positives.name = "num_positives"
            neg = train[train["target"] == 0].copy()
            # num_negatives = neg.groupby("user_id")["item_id"].count()
            # num_negatives.name = "num_negatives"
            # neg_sampling = pd.DataFrame(neg.groupby("user_id")["id"].apply(list)).join(
            #     num_positives, on="user_id", how="left"
            # )
            # neg_sampling = neg_sampling.join(num_negatives, on="user_id", how="left")
            # neg_sampling["num_negatives"] = (
            #     neg_sampling["num_negatives"].fillna(0).astype("int32")
            # )
            # neg_sampling["num_positives"] = (
            #     neg_sampling["num_positives"].fillna(0).astype("int32")
            # )
            # neg_sampling["num_choices"] = np.clip(
            #     neg_sampling["num_positives"] * self.num_neg_samples,
            #     a_min=0,
            #     a_max=25,
            # )

            on_date = datetime.strftime(
                train_interactions["datetime"].max(), "%Y-%m-%d"
            )
            add_genres_dummy_cosine(neg, on_date)
            neg = neg.sort_values(by=["user_id", "genres_dummy_cosine"], ascending=True)
            neg = neg.groupby("user_id").head(self.num_neg_samples)
            neg = neg.drop(columns="genres_dummy_cosine")

            # rng = default_rng(self.random_state)
            # neg_sampling["sampled_idx"] = neg_sampling.apply(
            #     lambda row: rng.choice(
            #         row["id"],
            #         size=min(row["num_choices"], row["num_negatives"]),
            #         replace=False,
            #     ),
            #     axis=1,
            # )
            # idx_chosen = neg_sampling["sampled_idx"].explode().values
            # neg = neg[neg["id"].isin(idx_chosen)].drop(columns="id")
            # train = train.drop(columns="id")
        elif self.sampling_strategy == SamplingStrategy.PER_POSITIVE:
            train["id"] = train.index
            num_positives = pos.groupby("user_id")["item_id"].count()
            num_positives.name = "num_positives"
            neg = train[train["target"] == 0]
            num_negatives = neg.groupby("user_id")["item_id"].count()
            num_negatives.name = "num_negatives"
            neg_sampling = pd.DataFrame(neg.groupby("user_id")["id"].apply(list)).join(
                num_positives, on="user_id", how="left"
            )
            neg_sampling = neg_sampling.join(num_negatives, on="user_id", how="left")
            neg_sampling["num_negatives"] = (
                neg_sampling["num_negatives"].fillna(0).astype("int32")
            )
            neg_sampling["num_positives"] = (
                neg_sampling["num_positives"].fillna(0).astype("int32")
            )
            neg_sampling["num_choices"] = np.clip(
                neg_sampling["num_positives"] * self.num_neg_samples,
                a_min=0,
                a_max=25,
            )

            rng = default_rng(self.random_state)
            neg_sampling["sampled_idx"] = neg_sampling.apply(
                lambda row: rng.choice(
                    row["id"],
                    size=min(row["num_choices"], row["num_negatives"]),
                    replace=False,
                ),
                axis=1,
            )
            idx_chosen = neg_sampling["sampled_idx"].explode().values
            neg = neg[neg["id"].isin(idx_chosen)].drop(columns="id")
            train = train.drop(columns="id")
        elif self.sampling_strategy == SamplingStrategy.PER_POSITIVE_POP_BALANCED:

            train["id"] = train.index
            num_positives = pos.groupby("user_id")["item_id"].count()
            num_positives.name = "num_positives"
            neg = train[train["target"] == 0].copy()
            num_negatives = neg.groupby("user_id")["item_id"].count()
            num_negatives.name = "num_negatives"

            # add num_positives for each item
            item_positives_count = pos.groupby("item_id")["target"].count()
            item_positives_count.name = "item_pos_count"
            neg = neg.join(item_positives_count, on="item_id")
            neg["item_pos_count"] = neg["item_pos_count"].fillna(1).clip(lower=1)

            # compute weights
            grouped = neg.groupby("user_id")
            result_df_list = []
            for _, group_df in grouped:
                group_df["sampling_proba"] = (
                    group_df["item_pos_count"] / group_df["item_pos_count"].sum()
                )
                result_df_list.append(group_df)
            neg = pd.concat(result_df_list)

            neg_sampling = (
                neg.groupby("user_id")
                .apply(lambda row: [list(row["id"]), list(row["sampling_proba"])])
                .apply(pd.Series)
                .rename(columns={0: "id", 1: "sampling_proba"})
            )
            neg_sampling = neg_sampling.join(num_positives, on="user_id", how="left")

            neg_sampling = neg_sampling.join(num_negatives, on="user_id", how="left")
            neg_sampling["num_negatives"] = (
                neg_sampling["num_negatives"].fillna(0).astype("int32")
            )
            neg_sampling["num_positives"] = (
                neg_sampling["num_positives"].fillna(0).astype("int32")
            )
            neg_sampling["num_choices"] = np.clip(
                neg_sampling["num_positives"] * self.num_neg_samples,
                a_min=0,
                a_max=25,
            )

            rng = default_rng(self.random_state)
            neg_sampling["sampled_idx"] = neg_sampling.apply(
                lambda row: rng.choice(
                    row["id"],
                    size=min(row["num_choices"], row["num_negatives"]),
                    replace=False,
                    p=row["sampling_proba"],
                ),
                axis=1,
            )
            idx_chosen = neg_sampling["sampled_idx"].explode().values
            neg = neg[neg["id"].isin(idx_chosen)]
            train = train.drop(columns="id")
            neg = neg[train.columns]
        else:
            raise ValueError(f"Sampling strategy {self.sampling_strategy} is not known")

        train_with_target = pd.concat([neg, pos], ignore_index=True).sample(
            frac=1, random_state=self.random_state
        )
        return train_with_target

    def _get_candidates_from_first_stage(
        self,
        recommenders: tp.Dict[str, ModelBase],
        users: ExternalIds,
        dataset: Dataset,
        filter_viewed: bool,
        items_to_recommend: tp.Optional[ExternalIds] = None,
    ) -> pd.DataFrame:

        candidates_dfs = []

        for model_name, recommender in recommenders.items():

            # Get candidates from model
            model_config = self.first_stage_models_config[model_name]
            candidates = recommender.recommend(
                users=users,
                dataset=dataset,
                k=model_config.num_candidates,
                filter_viewed=filter_viewed,
                items_to_recommend=items_to_recommend,
                add_rank_col=model_config.keep_ranks,
            )

            # Process ranks and scores as features
            rank_col_name, score_col_name = f"{model_name}_rank", f"{model_name}_score"
            if not model_config.keep_scores:
                candidates.drop(columns=Columns.Score, inplace=True)
            candidates.rename(
                columns={Columns.Rank: rank_col_name, Columns.Score: score_col_name},
                inplace=True,
                errors="ignore",
            )

            candidates_dfs.append(candidates)

        # Merge all candidates together and process missing ranks and scores
        all_candidates = reduce(
            lambda a, b: a.merge(b, how="outer", on=Columns.UserItem), candidates_dfs
        )
        first_stage_results = self._process_ranks_and_scores(all_candidates)

        return first_stage_results

    def _process_ranks_and_scores(
        self,
        all_candidates: pd.DataFrame,
    ) -> pd.DataFrame:

        for model_name, model_config in self.first_stage_models_config.items():
            rank_col_name, score_col_name = f"{model_name}_rank", f"{model_name}_score"
            if model_config.keep_ranks:
                all_candidates[rank_col_name] = all_candidates[rank_col_name].fillna(
                    model_config.ranks_fillna_value
                )
            if model_config.keep_scores:
                all_candidates[score_col_name] = all_candidates[score_col_name].fillna(
                    model_config.scores_fillna_value
                )

        return all_candidates

    def prepare_train_data_with_target(
        self,
        dataset: Dataset,
        calc_features_func: tp.Callable = calc_features_example,
        calc_features_func_kwargs: tp.Optional[tp.Dict[str, tp.Any]] = None,
    ) -> pd.DataFrame:
        """
        Preparing data in a format that is suitable for training second stage model. Splitting
        interactions, sampling positive and negative targets and feature calculation is provided
        Parameters
        ----------
        dataset : Dataset
            Dataset with input data.
            Usually it's the same dataset that was used to fit model.
        calc_features_func : tp.Callable, default calc_features_example
            Function that receives candidates dataframe, Dataset and optional kwargs and calculates
            all necessary features for second stage
        calc_features_kwargs : tp.Optional[tp.Dict[str, tp.Any]]
            Optional kwargs to pass for custom calc_features_func
        Returns
        -------
        pd.DataFrame
            Data table with columns `Columns.User`, `Columns.Item`, `target` and calculated features
        """
        logger = logging.getLogger("TWO STAGE MODEL")
        # Make history interactions for candidates generation and train interactions for training
        (
            history_interactions,
            train_interactions,
        ) = self._split_to_history_and_train_interactions(dataset.interactions.df)

        # Convert interactions ids to external
        # Train users are users that have interactions during second stage model train period
        train_interactions[Columns.User] = dataset.user_id_map.convert_to_external(
            train_interactions[Columns.User]
        )
        train_interactions[Columns.Item] = dataset.item_id_map.convert_to_external(
            train_interactions[Columns.Item]
        )
        train_users_ext = train_interactions[Columns.User].unique()

        # History dataset is a dataset which doesn't have interactions during train period
        history_dataset = replace_interactions_in_dataset(dataset, history_interactions)

        logger.info("Fitting first stage models to history dataset")
        train_recommenders = self._get_fitted_first_stage_recommenders(history_dataset)

        logger.info("Generating candidates")
        first_stage_candidates = self._get_candidates_from_first_stage(
            train_recommenders, train_users_ext, history_dataset, filter_viewed=True
        )

        logger.info("Preparing targets for candidates")
        candidates_with_target = self._set_targets_and_sample_negatives(
            first_stage_candidates, train_interactions
        )

        logger.info("Calculating features")
        if calc_features_func_kwargs is None:
            calc_features_func_kwargs = {}
        candidates_with_features_and_target = calc_features_func(
            candidates_with_target, history_dataset, **calc_features_func_kwargs
        )

        logger.info("Train with target prepared")

        return candidates_with_features_and_target

    def train(
        self,
        dataset: Dataset,
        calc_features_func: tp.Callable = calc_features_example,
        training_func: tp.Callable = training_example,
        calc_features_func_kwargs: tp.Optional[tp.Dict[str, tp.Any]] = None,
        training_func_kwargs: tp.Optional[tp.Dict[str, tp.Any]] = None,
    ) -> None:

        """Preparing data and fitting second stage model
         Parameters
         ----------
         dataset : Dataset
             Dataset with input data.
             Usually it's the same dataset that was used to fit model.
         calc_features_func : tp.Callable, default calc_features_example
             Function that receives candidates dataframe, Dataset and optional kwargs and calculates
             all necessary features for second stage
         training_func : tp.Callable, default training_example
             Function that receives train_with_target data and returns fitted second stage model
         calc_features_func_kwargs : tp.Optional[tp.Dict[str, tp.Any]]
             Optional kwargs to pass for custom calc_features_func
         training_func_kwargs : tp.Optional[tp.Dict[str, tp.Any]]
             Optional kwargs to pass for custom training_func
        Returns
         -------
         None
        """
        if calc_features_func_kwargs is None:
            calc_features_func_kwargs = {}
        train_with_target = self.prepare_train_data_with_target(
            dataset, calc_features_func, calc_features_func_kwargs
        )

        self.input_example = train_with_target.drop(
            [Columns.User, Columns.Item, "target"], axis=1
        ).head(5)

        if training_func_kwargs is None:
            training_func_kwargs = {}
        self.second_stage_model = training_func(train_with_target, training_func_kwargs)


class TwoStageModel(ModelBase):
    """Two Stage Model that has API identical to RecTools models"""

    def __init__(
        self,
        first_stage_models_config: FirstStageConfig,
        sampling_config: tp.Optional[SamplingConfig] = None,
        training_func: tp.Optional[tp.Callable] = None,
        calc_features_func: tp.Optional[tp.Callable] = None,
        verbose: int = 0,
        extra: tp.Optional[tp.Dict[str, tp.Any]] = None,
    ):
        super().__init__(verbose=verbose)

        self.model = TwoStageRecommender(
            first_stage_models_config=first_stage_models_config,
            sampling_config=sampling_config,
            verbose=verbose,
        )
        self.training_func = training_func
        self.calc_features_func = calc_features_func
        self.extra = extra

    def _fit(
        self, dataset: Dataset, *args, refit_first_stage_on_full: bool = True, **kwargs
    ):
        logger = logging.getLogger("TWO_STAGE_MODEL")
        if self.training_func is None:
            raise ValueError(
                "Training func needs to be specified for fitting TwoStageModel"
            )
        if self.calc_features_func is None:
            raise ValueError(
                "Calc features func needs to be specified for fitting TwoStageModel"
            )
        logger.info("Training")
        self.model.train(
            dataset=dataset,
            calc_features_func=self.calc_features_func,
            training_func=self.training_func,
            calc_features_func_kwargs={"extra": self.extra},
            training_func_kwargs=self.extra,
        )
        if refit_first_stage_on_full:
            logger.info("Fitting first stage models on full dataset")
            self.model.fit(dataset=dataset)

    def recommend(
        self,
        users: ExternalIds,
        dataset: Dataset,
        k: int,
        filter_viewed: bool,
        items_to_recommend: tp.Optional[ExternalIds] = None,
        add_rank_col: bool = True,
    ) -> pd.DataFrame:
        logger = logging.getLogger("TWO_STAGE_MODEL")
        logger.info("Recommending")
        if self.calc_features_func is None:
            raise ValueError(
                "Calc features func needs to be specified for recommending with TwoStageModel"
            )
        return self.model.recommend(
            users=users,
            dataset=dataset,
            k=k,
            filter_viewed=filter_viewed,
            items_to_recommend=items_to_recommend,
            add_rank_col=add_rank_col,
            calc_features_func=self.calc_features_func,
            calc_features_func_kwargs={"extra": self.extra},
        )
