# pylint: disable=attribute-defined-outside-init
"""Custom pyfunc model for mlflow for implicit itemm knn recommender with rectools wrapper"""

import typing as tp
from sys import version_info

import cloudpickle
import implicit
import mlflow
import numpy as np
import rectools
from implicit._nearest_neighbours import NearestNeighboursScorer
from implicit.nearest_neighbours import CosineRecommender, ItemItemRecommender
from mlflow.models.model import ModelInfo
from mlflow.pyfunc import PythonModel
from rectools import Columns
from rectools.dataset import Dataset, IdMap, Interactions
from rectools.models import ImplicitItemKNNWrapperModel
from scipy.sparse import csr_matrix

DEFAULT_DATETIME = "2022-01-01"  # Does not have any consequences at all, just a dummy
DEFAULT_USER = "_default_user"  # Does not have any consequences at all, just a dummy


class ImplicitOnlineModel(PythonModel):
    """Custom pyfunc model for mlflow for implicit itemm knn recommender with rectools wrapper"""

    def load_context(self, context):
        """This method is called when loading an MLflow model with pyfunc.load_model(), as soon as
        the Python Model is constructed.
        Args:
            context: MLflow context where the model artifact is stored.
        """
        with np.load(
            context.artifacts["implicit_model_path"], allow_pickle=False
        ) as data:
            loaded_recommender = ItemItemRecommender()
            if data.get("data") is not None:
                similarity = csr_matrix(
                    (data["data"], data["indices"], data["indptr"]), shape=data["shape"]
                )
                loaded_recommender.similarity = similarity
                loaded_recommender.scorer = NearestNeighboursScorer(similarity)
            loaded_recommender.K = int(data["K"])

            loaded_wrapper = ImplicitItemKNNWrapperModel(
                model=CosineRecommender(
                    K=loaded_recommender.K
                )  # TODO: add spport for other
            )
            loaded_wrapper.model = loaded_recommender
            loaded_wrapper.is_fitted = True

            self.model = loaded_wrapper
            self.item_id_map = IdMap(data["item_external_ids"])

    def predict(self, context, model_input):
        """Make recommendations for all users in interctions_df that is passed as `model_input`.

        Parameters
        ----------
        context : _type_
            Mlflow context
        model_input : pd.DataFrame
            Dataframe with external interactions ids. Recos will be made for all users in the df.
            If columns `num_recos` provided in df then maximum value from this columns will be taken
            as target number of recommendations from model. Else 20 recos will be computed

        Returns
        -------
        _type_
            Recommendations df
        """

        # Preparing dataset
        interactions_df = model_input.copy()
        if Columns.Weight not in interactions_df:
            interactions_df[Columns.Weight] = 1
        if Columns.Datetime not in interactions_df:
            interactions_df[Columns.Datetime] = DEFAULT_DATETIME
        if Columns.User not in interactions_df:
            interactions_df[Columns.User] = DEFAULT_USER
        user_id_map = IdMap.from_values(interactions_df[Columns.User])
        interactions = Interactions.from_raw(
            interactions_df, user_id_map, self.item_id_map
        )
        dataset = Dataset(user_id_map, self.item_id_map, interactions)

        # Defining number of items to recommend for each user
        if "num_recos" not in interactions_df.columns:
            k_recos = 20
        else:
            k_recos = interactions_df["num_recos"].max()

        # Recommending
        recos = self.model.recommend(
            users=user_id_map.external_ids,
            dataset=dataset,
            k=k_recos,
            filter_viewed=True,
        )
        return recos


def get_conda_env() -> tp.Dict[str, tp.Any]:
    """Create conda environment for recommender models

    Returns
    -------
    tp.Dict[str, tp.Any]
        Conda environment
    """
    conda_env = {
        "channels": ["defaults"],
        "dependencies": [
            f"python={version_info.major}.{version_info.minor}.{version_info.micro}",
            "pip",
            {
                "pip": [
                    "mlflow",
                    f"implicit=={implicit.__version__}",
                    f"rectools=={rectools.__version__}",
                    f"cloudpickle=={cloudpickle.__version__}",
                ],
            },
        ],
        "name": "two_stage_rectools_env",
    }
    return conda_env


def save_and_log_implicit_online_model(
    rectools_model: ImplicitItemKNNWrapperModel,
    model_path: str,
    item_id_map_external_ids: tp.Iterable,
    registry_model_name: tp.Optional[str] = None,
) -> tp.Optional[ModelInfo]:
    similarity_matrix = rectools_model.model.similarity
    args = {
        "K": rectools_model.model.K,
        "shape": similarity_matrix.shape,
        "data": similarity_matrix.data,
        "indptr": similarity_matrix.indptr,
        "indices": similarity_matrix.indices,
        "item_external_ids": item_id_map_external_ids,
    }
    np.savez(f"{model_path}.npz", **args)
    artifacts = {
        "implicit_model_path": f"{model_path}.npz",
    }
    conda_env = get_conda_env()
    mlflow.pyfunc.log_model(
        artifact_path=model_path,
        loader_module=None,
        data_path=None,
        code_path=None,
        python_model=ImplicitOnlineModel(),
        conda_env=conda_env,
        artifacts=artifacts,
    )
    if registry_model_name is None:
        return None
    run_id = mlflow.active_run().info.run_id
    model_uri = f"runs:/{run_id}/{model_path}"
    model_info = mlflow.register_model(
        model_uri,
        registry_model_name,
    )
    return model_info
