import logging
import typing as tp
from datetime import datetime

import pandas as pd
from catboost import CatBoostClassifier, Pool
from rectools import Columns
from rectools.dataset import Dataset
from sklearn.model_selection import train_test_split

from src.models.heuristics import add_genres_dummy_cosine
from src.models.tools import check_na_values_in_features


def merge_and_select_features(
    first_stage_candidates: pd.DataFrame,
    dataset: Dataset,  # pylint: disable=unused-argument
    on_date: str,
    features_config: tp.Dict[str, str],
) -> pd.DataFrame:

    users_static = pd.read_csv("data/processed/users_processed_static.csv")
    items_static = pd.read_csv("data/processed/items_processed_static.csv")
    users_time_based = pd.read_csv(f"data/processed/{on_date}/user_features.csv")
    items_time_based = pd.read_csv(f"data/processed/{on_date}/item_features.csv")

    result_candidates = first_stage_candidates

    if features_config["users_static"] is not None:
        result_candidates = result_candidates.join(
            users_static.set_index(Columns.User)[features_config["users_static"]],
            on=Columns.User,
            how="left",
        )

    if features_config["items_static"] is not None:
        result_candidates = result_candidates.join(
            items_static.set_index(Columns.Item)[features_config["items_static"]],
            on=Columns.Item,
            how="left",
        )

    if features_config["items_time_based"] is not None:
        result_candidates = result_candidates.join(
            items_time_based.set_index(Columns.Item)[
                features_config["items_time_based"]
            ],
            on=Columns.Item,
            how="left",
        )

    if features_config["users_time_based"] is not None:
        result_candidates = result_candidates.join(
            users_time_based.set_index(Columns.User)[
                features_config["users_time_based"]
            ],
            on=Columns.User,
            how="left",
        )
    if features_config["category_cols"] is not None:
        result_candidates[features_config["category_cols"]] = result_candidates[
            features_config["category_cols"]
        ].astype("category")

    # Check all features for missing values
    check_na_values_in_features(result_candidates)

    return result_candidates


def process_features_for_inference(features_prepared: pd.DataFrame) -> None:
    for num_bins in [3, 5, 10]:
        col_name = "share_of_watches_bin_" + str(num_bins)
        if col_name in features_prepared.columns:
            features_prepared[col_name] = features_prepared[col_name].replace(
                {num_bins - 1: num_bins - 2}
            )
    if "id_for_top" in features_prepared.columns:
        features_prepared["id_for_top"] = -1


def merge_and_select_features_defined(
    first_stage_candidates: pd.DataFrame,
    dataset: Dataset,
    extra: tp.Dict[str, tp.Any],
    for_inference: bool = False,
) -> pd.DataFrame:
    logger = logging.getLogger("TWO_STAGE_FEATURE_CALC")
    dataset_last_date = dataset.interactions.df[Columns.Datetime].max()
    dataset_last_date_str = datetime.strftime(dataset_last_date, "%Y-%m-%d")

    logger.info(f"Selecting features for dataset max date {dataset_last_date_str}")
    if extra is None:
        raise ValueError("features_config must be specified in `extra`")
    features_config = extra["features_config"]
    features_prepared = merge_and_select_features(
        first_stage_candidates, dataset, dataset_last_date_str, features_config
    )
    if for_inference:
        process_features_for_inference(features_prepared)

    return features_prepared


def features_func_defined_with_genres_dummy_cosine(
    first_stage_candidates: pd.DataFrame,
    dataset: Dataset,
    extra: tp.Dict[str, tp.Any],
    for_inference: bool = False,
) -> pd.DataFrame:
    features_prepared = merge_and_select_features_defined(
        first_stage_candidates, dataset, extra, for_inference
    )
    dataset_last_date = dataset.interactions.df[Columns.Datetime].max()
    dataset_last_date_str = datetime.strftime(dataset_last_date, "%Y-%m-%d")
    add_genres_dummy_cosine(features_prepared, dataset_last_date_str)
    return features_prepared


def train_catboost(
    train_with_target: pd.DataFrame,
    catboost_params: tp.Dict[str, tp.Any],
    sample_weight: tp.Optional[pd.Series] = None,
) -> CatBoostClassifier:

    train_with_target["weight"] = sample_weight

    positive_user_idx = train_with_target[train_with_target["target"] == 1][
        "user_id"
    ].unique()
    train_users, eval_users = train_test_split(
        positive_user_idx, test_size=0.1, random_state=345
    )
    train_with_target_main = train_with_target[
        train_with_target["user_id"].isin(train_users)
    ]
    train_with_target_eval = train_with_target[
        train_with_target["user_id"].isin(eval_users)
    ]

    x_train = train_with_target_main.drop(
        [Columns.User, Columns.Item, "target", "weight"], axis=1
    )
    y_train = train_with_target_main["target"].values
    weights_train = train_with_target_main["weight"].values
    x_eval = train_with_target_eval.drop(
        [Columns.User, Columns.Item, "target", "weight"], axis=1
    )
    y_eval = train_with_target_eval["target"].values
    weights_eval = train_with_target_eval["weight"].values

    train_pool = Pool(
        data=x_train,
        label=y_train,
        weight=weights_train,
        cat_features=catboost_params["cat_features"],
    )
    eval_pool = Pool(
        data=x_eval,
        label=y_eval,
        weight=weights_eval,
        cat_features=catboost_params["cat_features"],
    )

    second_stage_model = CatBoostClassifier(**catboost_params)
    second_stage_model.fit(
        train_pool,
        eval_set=eval_pool,
        early_stopping_rounds=200,
        plot=False,
    )

    return second_stage_model


def train_catboost_defined(
    train_with_target: pd.DataFrame, extra: tp.Dict[str, tp.Any]
) -> CatBoostClassifier:
    if extra is None:
        raise ValueError("features_config must be specified in `extra`")
    if "pop_quantile" in extra and extra["pop_quantile"] is not None:
        sample_weight = get_reduced_weight_for_popular_positives(
            train_with_target, pop_quantile=extra["pop_quantile"]
        )
    else:
        sample_weight = [1] * train_with_target.shape[0]
    return train_catboost(
        train_with_target, extra["catboost_params"], sample_weight=sample_weight
    )


def get_reduced_weight_for_popular_positives(
    train_with_target: pd.DataFrame, pop_quantile: float = 0.95
):
    pos = train_with_target[train_with_target["target"] == 1][
        ["user_id", "item_id", "target"]
    ].copy()
    num_watches = pos.groupby(by=Columns.Item)["target"].count()
    num_watches.name = "num_watches"
    pos = pos.join(num_watches, on="item_id", how="left")
    max_cumulative_weight = pos["num_watches"].quantile(pop_quantile)
    pos["corrected_weight"] = (max_cumulative_weight / pos["num_watches"]).clip(upper=1)
    weight = train_with_target.merge(
        pos, on=["user_id", "item_id", "target"], how="left"
    )
    weight["corrected_weight"] = weight["corrected_weight"].fillna(1)
    return weight["corrected_weight"]
