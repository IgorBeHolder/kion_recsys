import pathlib
import typing as tp

import mlflow
import yaml
from mlflow.models.model import ModelInfo
from rectools.dataset import Dataset

from src.models.implicit_online_model import save_and_log_implicit_online_model
from src.models.two_stage_recommender import FirstStageModel, TwoStageRecommender


def parse_two_stage_config(two_stage_config_path: str) -> tp.Dict[str, tp.Any]:
    with open(two_stage_config_path, "r", encoding="utf-8") as yamlfile:
        two_stage_config = yaml.load(yamlfile, Loader=yaml.FullLoader)[
            "two_stage_config"
        ]
    first_stage_config = {}
    for model_name, model_config in two_stage_config["first_stage_config"].items():
        first_stage_config[model_name] = FirstStageModel.parse_obj(model_config)
    two_stage_config["first_stage_config"] = first_stage_config
    catboost_params = two_stage_config["catboost_params"]
    catboost_params.update(
        {
            "cat_features": two_stage_config["features"]["category_cols"],
            "random_state": two_stage_config["sampling"]["random_state"],
        }
    )
    two_stage_config["catboost_params"] = catboost_params
    return two_stage_config


def mlflow_log_two_stage_config(two_stage_config: tp.Dict[str, tp.Any]) -> None:
    catboost_params = {
        key: value
        for key, value in two_stage_config["catboost_params"].items()
        if key
        not in ["verbose", "thread_count", "task_type", "cat_features", "devices"]
    }
    mlflow.log_params(catboost_params)
    mlflow.log_params(two_stage_config["sampling"])
    mlflow.log_param(
        "first_stage_models",
        ", ".join(list(two_stage_config["first_stage_config"].keys())),
    )
    for feature_type, features_list in two_stage_config["features"].items():
        if features_list is not None:
            mlflow.log_param(
                f"features_{feature_type}",
                ", ".join(two_stage_config["features"][feature_type]),
            )


def save_log_and_register_two_stage_online_model(
    recommender: TwoStageRecommender,
    dataset_full: Dataset,
    registry_model_name: str,
    first_stage_registry_model_prefix: str = "first_stage_",
    first_stage_folder_path: str = "first_stage",
    second_stage_folder_path: str = "second_stage",
) -> tp.Optional[ModelInfo]:

    pathlib.Path(first_stage_folder_path).mkdir(parents=True, exist_ok=True)
    pathlib.Path(second_stage_folder_path).mkdir(parents=True, exist_ok=True)

    # Save locally, log to mlflow and register first stage models
    first_stage_models_list: tp.List[str] = []
    for model_name in recommender.first_stage_models_config:
        model_path = f"{first_stage_folder_path}/{model_name}"
        model_registry_model_name = f"{first_stage_registry_model_prefix}{model_name}"
        model_info = save_and_log_implicit_online_model(
            rectools_model=recommender.recommenders[model_name],
            model_path=model_path,
            item_id_map_external_ids=dataset_full.item_id_map.external_ids,
            registry_model_name=model_registry_model_name,
        )
        if model_info is not None:
            first_stage_models_list.append(f"{model_info.name}/{model_info.version}")
    registered_first_stage_models = ", ".join(first_stage_models_list)
    mlflow.log_param("registered_first_stage_models", registered_first_stage_models)

    # Save locally, log to mlflow and register second stage model
    model_path = f"{second_stage_folder_path}/model"
    mlflow.catboost.log_model(
        cb_model=recommender.second_stage_model,
        artifact_path=model_path,
        input_example=recommender.input_example,
    )
    model_uri = f"runs:/{mlflow.active_run().info.run_id}/{model_path}"
    model_info = mlflow.register_model(model_uri, registry_model_name)

    return model_info
