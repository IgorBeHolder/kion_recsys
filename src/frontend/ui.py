# pylint: disable=unsubscriptable-object, invalid-name, bare-except
# flake8: noqa W291
from collections import OrderedDict
from typing import Dict, List, Union

import pandas as pd
import requests
import streamlit as st
from dotenv import load_dotenv
from PIL import Image
from st_pages import Page, add_page_title, show_pages
from streamlit_extras.stoggle import stoggle
from streamlit_extras.switch_page_button import switch_page
from streamlit_toggle import st_toggle_switch

load_dotenv()

N_COLS = 3  # Amount of columns in films history selection
N_COLS_IMG = 4  # Amount of colums in image grid
N_RECOS = 12  # Amount of recommendations on page
PREVIEW_WIDTH = 150  # Width of selected items posters
MAX_HISTORY_SIZE = 5  # Max amount of input items

# Note: always left `unknown` as the last option. We use last index value as default
SEX_MAP = OrderedDict(
    [("Мужской", "M"), ("Женский", "F"), ("Не указан", "sex_unknown")]
)
AGE_MAP = OrderedDict(
    [
        ("от 18 до 25", "age_18_24"),
        ("от 25 до 35", "age_25_34"),
        ("от 35 до 45", "age_35_44"),
        ("от 45 до 55", "age_45_54"),
        ("от 55 до 65", "age_55_64"),
        ("более 65", "age_65_inf"),
        ("Не указан", "age_unknown"),
    ]
)
INCOME_MAP = OrderedDict(
    [
        ("менее 20", "income_0_20"),
        ("от 20 до 40", "income_20_40"),
        ("от 40 до 60", "income_40_60"),
        ("от 60 до 90", "income_60_90"),
        ("от 90 до 150", "income_90_150"),
        ("более 150", "income_150_inf"),
        ("Не указан", "income_unknown"),
    ]
)
DEFAULT_MOVIES = [
    10772,  # Зеленая книга
    1931,  # Мейр из Истауеа
    12048,  # Настоящий детектив
    3343,  # Ла-ла Лэнд
    10073,  # Джоун Уик 3
    4436,  # Ford против Ferrari
]


# ----------------------------- Helper functions -----------------------------#


@st.cache_data
def get_film_titles() -> pd.Series:
    payload: Dict[str, str] = {}
    result_json = requests.get(
        "http://model_service:8003/get_films_title_with_year",
        timeout=300,
        params=payload,
    ).json()
    result_df = pd.read_json(result_json, orient="index")
    result_df = result_df.set_index("item_id")  # pylint: disable=no-member
    return result_df["title"]


@st.cache_data
def get_models_ordered_map() -> "OrderedDict[str, str]":
    payload: Dict[str, str] = {}
    result_json = requests.get(
        "http://model_service:8003/get_models_list_for_ui",
        timeout=300,
        params=payload,
    ).json()
    models_ordered_dict = OrderedDict(result_json)
    return models_ordered_dict


def get_recs(
    item_ids: List[int],
    model_name: str,
    num_recos: int,
    user_features: Dict[str, Union[str, bool]],
) -> List[int]:
    user_data = {"item_ids": {"ids": item_ids}, "features": user_features}
    payload = {
        "num_recos": num_recos,
    }
    result_json = requests.post(
        f"http://model_service:8003/recommend_with_model/{model_name}",
        json=user_data,
        timeout=300,
        params=payload,  # type: ignore
    ).json()
    return result_json


def get_items_features(item_ids: List[int]) -> pd.DataFrame:
    result_json = requests.post(
        "http://model_service:8003/get_features_for_item_ids",
        json={"ids": item_ids},
        timeout=300,
    ).json()
    items_features = pd.read_json(result_json, orient="index")
    items_features = items_features[
        [
            "item_id",
            "title",
            "title_orig",
            "release_year",
            "watched_in_all_time",
            "genres",
            "countries",
            "content_type",
        ]
    ]
    items_features = items_features.set_index("item_id")
    items_features = items_features.loc[item_ids, :]
    return items_features


@st.cache_data
def get_default_idx_for_item_ids(
    item_ids: List[int], film_titles: pd.Series
) -> List[int]:
    film_titles_df = pd.DataFrame(film_titles).reset_index()
    idx = []
    for item_id in item_ids:
        index = film_titles_df[film_titles_df["item_id"] == item_id].index[0]
        idx.append(int(index))
    return idx


def get_poster_url(item_id: Union[str, int]) -> str:
    return f"https://recsysart.ru/posters/{item_id}.jpg"


# ----------------------------------------------------------------------------#


st.set_page_config(
    page_title="RecSys Art - онлайн рекомендации: двухэтапная модель на основе датасета Kion",
    page_icon="🎥",
    layout="wide",
)

show_pages(
    [
        Page("ui.py", "Онлайн рекомендации", ":movie_camera:"),
        Page(
            "streamlit_pages/offline_recs.py", "Метрики и визуальный анализ", ":books:"
        ),
        Page("streamlit_pages/eda.py", "Анализ данных датасета Kion", ":bar_chart:"),
        Page("streamlit_pages/about.py", "Детали реализации", ":rocket:"),
    ]
)

add_page_title()

try:
    film_titles = get_film_titles()
    default_movie_idx = get_default_idx_for_item_ids(DEFAULT_MOVIES, film_titles)
except requests.ConnectionError:
    st.error(
        "Сервис онлайн рекомендаций в данный момент недоступен. Попробуйте перезагрузить страницу"
    )
    st.stop()


model_map = get_models_ordered_map()


st.caption(
    """RecSys Art: Machine Learning, Data Science, RecSys & MLOps playground for fun, challenge 
    and experience"""
)
st.markdown("###  Рекомендации фильмов на основе датасета Kion:")


num_inputs = int(
    st.number_input(
        label="Число просмотренных фильмов",
        min_value=0,
        max_value=MAX_HISTORY_SIZE,
        step=1,
        value=3,
    )
)

# Create grid with selection drop-down lists
selected_film_titles = []
if num_inputs > 0:
    n_rows = num_inputs // N_COLS
    remaining_cols = num_inputs % N_COLS
    for row_idx in range(n_rows + 1):
        cols = st.columns(N_COLS)
        for col_idx in range(N_COLS):
            with cols[col_idx]:
                if row_idx < n_rows or (row_idx == n_rows and col_idx < remaining_cols):
                    film_title = st.selectbox(
                        label="Выберите фильм:",
                        options=film_titles.tolist(),
                        key=f"selectbox_{row_idx}_{col_idx}",
                        index=default_movie_idx[row_idx * N_COLS + col_idx],
                    )
                    selected_film_titles.append(film_title)

# Show posters row
cols = st.columns(MAX_HISTORY_SIZE)
for idx in range(num_inputs):
    with cols[idx]:
        film_title = selected_film_titles[idx]
        selected_id = film_titles[film_titles == film_title].index[0]
        st.image(get_poster_url(selected_id), width=PREVIEW_WIDTH)

unique_film_titles = set(selected_film_titles)

show_user_features = st_toggle_switch(
    label="Добавить личные данные (для модели v.0)",
    key="switch_1",
    default_value=False,
    label_after=True,
)

placeholder = st.empty()
if show_user_features:
    with placeholder.container():
        col1, col2 = st.columns(2)
        with col1:
            age = st.selectbox(
                label="Возраст (лет)",
                options=list(AGE_MAP.keys()),
                index=len(AGE_MAP) - 1,
            )
            income = st.selectbox(
                label="Доход (тысяч рублей)",
                options=list(INCOME_MAP.keys()),
                index=len(INCOME_MAP) - 1,
            )
        with col2:
            sex = st.selectbox(
                label="Пол", options=list(SEX_MAP.keys()), index=len(SEX_MAP) - 1
            )
            st.markdown("Наличие детей")
            kids_flg = st.checkbox(label="Есть дети", value=False)
else:
    age = "Не указан"
    sex = "Не указан"
    income = "Не указан"
    kids_flg = False

# Map user feature names with actual values
user_features: Dict[str, Union[str, bool]] = {
    "age": AGE_MAP[str(age)],
    "income": INCOME_MAP[str(income)],
    "sex": SEX_MAP[str(sex)],
    "kids_flg": kids_flg,
}


model_type = st.radio(
    label="Выберите модель",
    options=list(model_map.keys()),
    label_visibility="collapsed",
)

reco_df = None  # pylint: disable=invalid-name
if st.button(label="🍿 Получить рекомендации фильмов"):
    if len(unique_film_titles) == 0:
        st.markdown("Не выбрано ни одного фильма!")
    else:
        selected_ids = film_titles[film_titles.isin(unique_film_titles)].index.tolist()
        rec_ids = get_recs(
            item_ids=selected_ids,
            model_name=model_map[str(model_type)],
            num_recos=N_RECOS,
            user_features=user_features,
        )
        reco_df = get_items_features(rec_ids)

# Create poster annotation
if reco_df is not None:
    annots = []
    for index, row in reco_df.iterrows():
        text = f"""
                {row["title"]}, {row["release_year"]} <br>
                * {int(row["watched_in_all_time"]):,} просмотров
                * {row["genres"]}
                * {row["countries"]}
            """
        annots.append(text)

# Get poster URL
if reco_df is not None:
    img_urls = []
    for index, row in reco_df.iterrows():
        img_url = get_poster_url(index)
        img_urls.append(img_url)

# Create poster grid
if reco_df is not None:
    n_rows = len(reco_df) // N_COLS_IMG

    for row_idx in range(n_rows):
        start_idx = row_idx * N_COLS_IMG
        end_idx = start_idx + N_COLS_IMG

        urls = img_urls[start_idx:end_idx]
        titles = annots[start_idx:end_idx]

        cols = st.columns(N_COLS_IMG)

        for idx, (url, title) in enumerate(zip(urls, titles)):
            with cols[idx]:
                st.image(Image.open(requests.get(url, stream=True, timeout=300).raw))
                st.markdown(title, unsafe_allow_html=True)


st.write("")
st.write("")
st.divider()

stoggle(
    "Какие модели использованы в рекомендательной системе?",
    """
    <br>
    <b>Двухэтапная модель v.1, наш выбор для рекомендаций</b>: Кандидатов генерируем моделью, 
    основанной на методе ближайших
    соседей по расстояниям
    между векторами айтемов в матрице интеракций (метрика расстояния: bm25). Гиперпараметры
    подобраны для баланса между метриками релевантности и предсказательной силы (основные метрики: 
    serendipity, recall без популярных айтемов) с проверкой визуальным анализом. Для
    отобранных кандидатов считаем косинусную близость дамми вектора жанров к среднему
    дамми вектору айтемов по истории юзера. Финальный ранк взвешиваем между ранком от модели
    первого этапа и ранком по косинусной близости жанров с весом 1:2. Это модель, которую
    мы выбрали для формирования качественных рекомендаций.
    <br><br>
    <b>Двухэтапная модель v.0, 4 место в соревновании Kion</b>: кандидатов генерируем моделью
    коллаборативной фильтрации (на этот раз метрика расстояния: косинус).
    Качество ранжирования поднимаем эвристиками: 
    забываем слишком 
    длинную или слишком давнюю истории взаимодействий юзеров и фильтруем из выдачи непопулярные 
    айтемы. Список кандидатов обогащаем фичами юзера и
    предрассчитанными фичами айтемов. Учитываем различные характеристики популярности айтемов
    (наклон тренда, просмотры за неделю и за все время, давность 95% квантиля и медианы
    распределения дат просмотров), характеристики аудитории фильма (доля молодой аудитории, доля 
    женской
    аудитории) и простые фичи (тип контента, страна производства, жанры). Список
    выбранных кандидатов с заполненными фичами подаем модели градиентного бустинга (catboost) для
    реранжирования. Модель повторяет решение, занявшее 4 место в 
    соревновании по датасету Kion от ods и MTS. Часто выдает нерелевантные, но популярные айтемы,
    за счет чего имеет высокие показатели по метрикам ранжирования. Из-за сильного popularity bias
    практически бесполезна для реального использовании. <br><br>
     <b>Коллаборативная фильтрация, бейзлайн</b>: Модель,
    основанная на методе ближайших соседей по расстояниям
    между векторами айтемов в матрице интеракций (метрика расстояния: косинус). Модель первого этапа 
    для Двухэтапной модели v.0. Может подбирать удачные рекомендации, но также периодически
    рекомендует и слишком популярные айтемы, и явных аутсайдеров.
    """,
)
stoggle(
    "В чем проблема рекомендательных моделей с высокими метриками ранжирования?",
    """
    Метрики ранжирования можно хорошо поднять, просто подмешивая популярные айтемы в начало
    выдачи. Поскольку распределение популярности айтемов часто следует степенному закону,
    несколько айтемов-хитов имеют значительное превосходство над основным каталогом. И вероятность
    взаимодействия юзеров с такими хитами сопоставима или выше, чем с айтемами, которые были бы
    действительно релевантны.<br>
    Из-за этого у моделей, построенных на максимизации метрик MAP, Recall, Hit rate и подобных,
    скорее всего будет сильный перекос в сторону рекомендации популярных (но возможно нерелевантных) 
    айтемов. Для диагностики можно смотреть на метрики popularity bias, которые легко
    интерпретировать, например: пересечение рекомендаций с популярным
    алгоритмом либо доля юзеров, которым выдается самый популярный айтем на первых позициях.<br> 
    Использование бустинга в качестве модели второго этапа тоже может добавить проблем. Причина в 
    том, что бустинг, решая задачу классификации, при неуверенных скорах
    от первой модели, сразу продвигает вперед айтемы, которые с большей вероятностью окажутся
    в просмотрах: например, хиты последней недели. Для ряда бизнес задач это может быть серьезным
    недостатком. Такое поведение можно фиксить стратегией семплирования или подбором весов в
    лосс-функции, но мы для нашей задачи отказались от бустинга при построении финального 
    решения, сменив его на content-based реранжирование.
    """,
)

st.divider()
st.markdown("Больше интересного:")

switch_offline = st.button(
    ":books: Метрики и визуальный анализ: эксперименты с моделями"
)
if switch_offline:
    switch_page("Метрики и визуальный анализ")

switch_eda = st.button(":bar_chart: Анализ данных датасета Kion: инсайты")
if switch_eda:
    switch_page("анализ данных датасета kion")

switch_real = st.button(
    ":rocket: Детали реализации: двухэтапная модель, микросервисы, DVC"
)
if switch_real:
    switch_page("детали реализации")
