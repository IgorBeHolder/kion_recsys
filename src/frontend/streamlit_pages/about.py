# flake8: noqa W291

import streamlit as st
from st_pages import add_page_title
from streamlit_embedcode import gitlab_snippet
from streamlit_extras.mention import mention
from streamlit_extras.switch_page_button import switch_page

st.set_page_config(
    page_title="RecSys Art - Детали реализации: двухэтапная модель, микросервисы, DVC",
    page_icon="🎥",
    layout="wide",
)
add_page_title()

st.caption(
    """RecSys Art: Machine Learning, Data Science, RecSys & MLOps playground for fun, 
challenge and experience"""
)

# ------------------------- Page elements functions -------------------------- #


def two_stage_realisation():
    st.markdown(
        "Один класс и один конфиг на все случаи использования двухэтапной архитектуры:"
    )
    col1, col2 = st.columns(2)
    with col1:
        gitlab_snippet(
            "https://gitlab.com/-/snippets/2548174",
            width=400,
            height=350,
        )
    with col2:
        st.markdown(
            """
        - Кросс-валидация
        - Обучения в Production и логирование в MlFlow
        - Оффлайн рекомендации
        - Онлайн рекомендации
        """
        )


def stack():
    st.divider()
    st.subheader("Стек технологий")
    col1, col2, col3 = st.columns(3)
    with col1:
        st.markdown("**Deploy & Workflow**")
        st.write(
            '<hr style="background-color: #FF0000; margin-top: 0;'
            ' margin-bottom: 0; height: 3px; border: none; border-radius: 3px;">',
            unsafe_allow_html=True,
        )
        st.markdown(
            """
        Deploy:  
        - Docker  
        - Gitlab CI
        - Poetry

        Workflow:  
        - DVC
        - Snakemake
        """
        )
    with col2:
        st.markdown("**Services in Docker**")
        st.write(
            '<hr style="background-color: #00BFFF; margin-top: 0;'
            ' margin-bottom: 0; height: 3px; border: none; border-radius: 3px;">',
            unsafe_allow_html=True,
        )
        st.markdown(
            """
        - FastAPI
        - Nginx
        - Minio
        - MlFlow
        - PostgreSQL
        - Streamlit
        - Voila
        """
        )
    with col3:
        st.markdown("**ML & RecSys**")
        st.write(
            '<hr style="background-color: #FFE033; margin-top: 0;'
            ' margin-bottom: 0; height: 3px; border: none; border-radius: 3px;">',
            unsafe_allow_html=True,
        )
        st.markdown(
            """
        ML & RecSys:  
        - Implicit
        - Catboost
        - RecTools  
        - Scikit-learn

        Visualization:  
        - Plotly
        - Ipywidgets
        """
        )


def authors():
    st.subheader("Авторы проекта")
    col1, col2 = st.columns(2)
    with col1:
        st.markdown(
            """
        **Тихонович Дарья**: лидер проекта, ведущий разработчик  
        ML Инженер @ MTS BigData, группа рекомендательных систем
        """
        )
        mention(
            label="LinkedIn",
            icon="http://recsysart.ru/images/logos/linkedin.png",
            url="https://www.linkedin.com/in/blondered/",
        )
        mention(
            label="Telegram",
            icon="http://recsysart.ru/images/logos/telegram.png",
            url="https://t.me/blondered",
        )
    with col2:
        st.markdown(
            """
        **Гусаров Григорий**: разработчик  
        Участвовал во всех аспектах работы над проектом (ML workflow, RecSys, фронтэнд, бэкэнд)
        """
        )
        mention(
            label="LinkedIn",
            icon="http://recsysart.ru/images/logos/linkedin.png",
            url="https://www.linkedin.com/in/gusarovgrigoriy/",
        )
        mention(
            label="Telegram",
            icon="http://recsysart.ru/images/logos/telegram.png",
            url="https://t.me/Gooogr",
        )


mention(
    label="Репозиторий проекта",
    icon="http://recsysart.ru/images/logos/gitlab.png",
    url="https://gitlab.com/recsysart/kion_recsys",
)

# --------------------------------------------------------------------------- #

authors()

st.divider()
st.subheader("Архитектура приложения")
st.markdown(
    """Приложение работает на базе микросервисов, развернутых в Docker контейнерах.  Обучение
    моделей реализовано с помощью DVC пайплайнов."""
)
st.image("http://recsysart.ru/images/services_scheme.jpg")
with st.expander("Узнать больше"):
    st.markdown(
        """
    - Рекомендации рассчитываются с помощью Model service API, реализованного на FastAPI. Обученные
    модели и настройки двухэтапной архитектуры подгружаются в сервис из MlFlow.
    - Фронтэнд 
    написан на Streamlit, а также включает в себя через iframe отображение Jupyter ноутбука,
    развернутого с помощью Voila.
    - Маршрутизация и сервинг статичного контента реализован с помощью Nginx
    - Модели хранятся в s3 хранилище Minio, а база данных PostgresSQL хранит все данные от логов
    MlFlow до фичей айтемов, необходимых для расчета рекомендаций.
    - Пайплайны обработки данных и обучения моделей реализованы с помощью DVC и рассчитываются на
    отдельном хосте, а результаты логируются в MlFlow. Рекомендации сохраняются в базу данных
    для проведения визуального анализа.
    - Постеры для фильмов были подобраны парсингом API Кинопоиска и сохранены в Minio.
    """
    )


stack()

st.divider()
st.subheader("Двухэтапная рекомендательная модель")
st.markdown(
    """В проекте реализованы онлайн рекомендации и пайплайны экспериментов для двухэтапной 
рекомендательной модели"""
)
st.image("http://recsysart.ru/images/2_stage.jpg")
with st.expander("Узнать больше"):
    st.markdown(
        """
    - Двухэтапные рекомендации предполагают, что кандидаты генерируются любыми рекомендательными
    алгоритмами (от эвристик до нейронных сетей), а затем обогащаются дополнительными фичами и
    реранжируются.
    - Для генерации кандидатов во время обучения от интеракций "отрезают" период, который модели
    первого этапа не видят. На предшествующем датасете модели генерируют кандидатов.
    - Задачу реранжирования чаще всего выполняет градиентный бустинг. Для всех сгенерированных
    кандидатов проставляются таргеты в зависимости от того, взаимодействовали ли юзеры
    с рассчитанными для них кандидатами (1 - было взаимодейтсвие, 0 - не было) во время
    "отрезанного" периода. Посколько негативных
    таргетов значительно больше, чаще всего их семплируют. 
    - На собранной выборке бустинг обучают решать задачу классификации либо ранжирования.
    - Обученная модель умеет получать на вход кандидатов с нового временного промежутка (например,
    со всех интеракций) и 
    проставлять им скоры, на основе которых айтемы можно сортировать, получая таким образом 
    финальный список рекомендаций.
    """
    )

st.divider()
st.subheader("Наша реализация двухэтапной модели")
two_stage_realisation()
with st.expander("Узнать больше"):
    st.markdown(
        """
        **Фичи:**
        - Поддержка любого количества моделей первого этапа
        - Использование скоров и ранков от моделей первого этапа
        - Поддержка моделей из RecTools или оберток с тем же API
        - Кастомный второй этап (любая модель, любой скрипт обучения)
        - Кастомные функции для расчета/загрузки фичей
        """
    )

st.divider()
st.subheader("Онлайн рекомендации c двухэтапной моделью")
st.image("http://recsysart.ru/images/online_reco.jpg")
with st.expander("Узнать больше"):
    st.markdown(
        """
    Для онлайн рекомендаций все обученные модели двухэтапной архитектуры необходимо сохранить в
    Model Registry (мы используем MlFlow с подключенным Minio). Во время загрузки API подгружает
    модели и разворачивает двухэтапную архитектуру согласно сохраненному вместе с моделями конфигу.
    При получении запроса со стороны фронтэнда API запускает рекомендательную систему.  
    На первом этапе Implicit KNN генерирует кандидатов. Дальше к ним добавляются фичи айтемов,
    предрассчитанные и сохраненные в базу данных, и фичи юзера (если они были указаны при запросе).
    Собранная выборка подается модели второго этапа. В нашем случае это обученный Catboost с 
    классификационным лоссом. Катбуст рассчитывает вероятности взаимодействия юзера с предложенными
    кандидатами. И на основе вероятностей API выдает список рекомендаций на фронтэнд.
    """
    )


st.divider()
st.subheader("Автоматизированные пайплайны экспериментов с DVC")
st.markdown(
    """
Мы написали объемные пайплайны для того, чтобы облегчить любые эксперименты с
    двухэтапной архитектурой (кросс-валидация, визуальный анализ и вывод в Production):
"""
)
st.image("http://recsysart.ru/images/workflow.jpg")
with st.expander("Узнать больше"):
    st.markdown(
        """
    - Предрасчет любых фичей на все необходимые даты для кросс-валидации и инференса
    - Перезапуск только необходимых ступеней пайплайнов в зависимости от изменений кода или данных
    в репозитории
    - Полная воспроизводимость
    - Мгновенная возможность сравнить рекомендации от разных алгоритмов визуально
    - Быстрая подготовка любой модели к запуску в онлайн
    """
    )

st.divider()
st.subheader("Таймлайн кросс-валидации в экспериментах")
st.markdown(
    """Кросс-валидация двухэтапной модели предполагает сложную структуру работы с таймлайном интеракций
    в датасете:"""
)
st.image("http://recsysart.ru/images/crossval_timeline.jpg")
with st.expander("Узнать больше"):
    st.markdown(
        """
    - Каждый фолд имеет собственный период тестовых взаимодействий. На них будут считаться метрики
    качества рекомендаций обученной модели
    - До тестового периода каждый фолд имеет период обучения модели второго этапа. С этого периода
    берутся таргеты для обучения модели
    - До времени обучения модели второго этапа все интеракции относятся к периоду обучения моделей
    первого этапа для генерации кандидатов.  
    Итоговая логика каждого фолда:
    - Обучаем модели первого этапа на своем периоде, генерируем кандидатов
    - Подгружаем фичи, рассчитанные на ту же дату
    - Собираем таргеты с периода обучения модели второго этапа, обучаем и сохраняем модель
    - Снова обучаем модели первого этапа, на всех интеракциях до тестового периода. Генерируем
    кандидатов
    - Подгружаем фичи, рассчитанные на новую дату
    - Рассчитываем рекомендации обученной моделью второго этапа и считаем метрики на тестовом 
    периоде
    """
    )

st.divider()
st.markdown("Больше интересного:")

switch_online = st.button(":movie_camera: Онлайн рекомендации: двухэтапная модель")
if switch_online:
    switch_page("онлайн рекомендации")

switch_offline = st.button(
    ":books: Метрики и визуальный анализ: эксперименты с моделями"
)
if switch_offline:
    switch_page("Метрики и визуальный анализ")

switch_eda = st.button(":bar_chart: Анализ данных датасета Kion: инсайты")
if switch_eda:
    switch_page("анализ данных датасета kion")
