# flake8: noqa W291

import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import requests
import streamlit as st
from st_pages import add_page_title
from streamlit_extras.switch_page_button import switch_page

st.set_page_config(
    page_title="RecSys Art - анализ данных датасета Kion", page_icon="🎥", layout="wide"
)
add_page_title()

st.caption(
    """RecSys Art: Machine Learning, Data Science, RecSys & MLOps playground for fun, 
challenge and experience"""
)

# ------------------------- Data caching functions -------------------------- #


@st.cache_data
def get_top_100_items() -> pd.DataFrame:
    result_json = requests.get(
        "http://model_service:8003/get_top_100_items",
        timeout=300,
    ).json()
    result_df = pd.read_json(result_json, orient="index")
    result_df["content_type"] = result_df["content_type"].map(
        {"film": "Фильм", "series": "Сериал"}
    )
    return result_df


@st.cache_data
def get_top_100_weekly_items(content_type) -> pd.DataFrame:
    result_json = requests.get(
        "http://model_service:8003/get_top_100_weekly_stats",
        timeout=300,
        params={"content_type": content_type},
    ).json()
    result_df = pd.read_json(result_json, orient="index")
    return result_df


@st.cache_data
def get_statistic_per_county_and_genre() -> pd.DataFrame:
    result_json = requests.get(
        "http://model_service:8003/get_statistic_per_county_and_genre",
        timeout=300,
    ).json()
    result_df = pd.read_json(result_json, orient="index")
    result_df["content_type"] = result_df["content_type"].map(
        {"film": "Фильмы", "series": "Сериалы"}
    )
    return result_df


# --------------------------------------------------------------------------- #

# ------------------------ Chart plotting functions ------------------------- #


def plot_top_100(chart_data: pd.DataFrame, title: str) -> go.Figure:
    fig = go.Figure()
    fig.add_trace(
        go.Bar(
            x=chart_data.index,
            y=chart_data["cnt"],
            # text=chart_data['title'],
            customdata=chart_data[["title", "content_type", "cnt"]],
            hovertemplate="<br>".join(
                [
                    "Название: %{customdata[0]}",
                    "Категория:  %{customdata[1]}",
                    "Число просмотров: %{customdata[2]:,}<extra></extra>",
                ]
            ),
            marker=dict(
                color=chart_data["cnt"],
                colorscale="Plasma",
                colorbar=dict(
                    orientation="h",
                    title="",
                    thickness=20,
                    len=1,
                    yanchor="top",
                    y=-0.1,
                    ticks="outside",
                    ticklen=10,
                ),
            ),
        )
    )

    fig.update_layout(
        title=title,
        xaxis=dict(title="X axis name", visible=False),
        yaxis=dict(title="Число просмотров"),
        showlegend=False,
    )
    return fig


def plot_sunburts(chart_data: pd.DataFrame, title: str) -> go.Figure:
    fig = px.sunburst(
        chart_data,
        path=["main_country", "content_type", "main_genre"],
        values="cnt",
        color="main_country",
        color_discrete_sequence=px.colors.sequential.Plasma,
        title=title,
        custom_data=chart_data[["cnt", "main_country", "content_type", "main_genre"]],
        width=1024,
        height=800,
    )
    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "Число просмотров: %{customdata[0]:,}",
                "Страна: %{customdata[1]}",
                "Категория: %{customdata[2]}",
                "Жанр: %{customdata[3]}<extra></extra>",
            ]
        ),
    )
    return fig


def plot_time_series(chart_data: pd.DataFrame, title: str) -> go.Figure:
    fig = px.line(
        data_frame=chart_data,
        x="week",
        y="cnt",
        color="item_id",
        color_discrete_sequence=px.colors.qualitative.Plotly,
        title=title,
        hover_data=["title", "week", "cnt"],
    )

    fig.update_xaxes(title="Неделя")
    fig.update_yaxes(title="Число просмотров")
    fig.update_traces(
        hovertemplate="<br>".join(
            [
                "Название: %{customdata[0]}",
                "Число просмотров: %{y}",
                "Неделя: %{x}<extra></extra>",
            ]
        ),
    )
    fig.update_layout(showlegend=False)
    return fig


# --------------------------------------------------------------------------- #

st.markdown(
    """В проекте мы работали с открытым датасетом онлайн-кинотеатра Kion. Ниже представлены 
ключевые инсайты с разведочного анализа
данных"""
)

general_info = get_statistic_per_county_and_genre()
st.plotly_chart(
    plot_sunburts(
        general_info, "Предпочтения пользователей в отношении характеристик айтемов"
    ),
    use_container_width=True,
)

with st.expander("Описание графика"):
    st.markdown(
        """Солидная доля просмотров приходится на фильмы и сериалы российского производства,
    причем как фильмы, так и сериалы интересны пользователям в равной степени. В основном юзеры
    выбирают драмы, комедии и триллеры. Также ключевая доля просмотров приходится на фильмы
    производства США: боевики, драмы, мультфильмы, фантастика. Далее следуют фильмы европейского
    производства, в основном боевики и драмы."""
    )

top_100_items_df = get_top_100_items()
st.plotly_chart(
    plot_top_100(
        top_100_items_df, "Распределение популярности айтемов: степенной закон"
    ),
    use_container_width=True,
)

with st.expander("Описание графика"):
    st.markdown(
        """
    Популярность айтемов распределена по степенному закону и имеет длинный правый хвост, что 
    является классикой для онлайн кинотеатров. Несколько айтемов значительно доминируют в 
    просмотрах пользователей.
    """
    )

top_100_weekly_films_df = get_top_100_weekly_items("film")
top_100_weekly_series_df = get_top_100_weekly_items("series")
st.plotly_chart(
    plot_time_series(
        top_100_weekly_films_df, "Динамика просмотров топ 100 фильмов по неделям"
    ),
    use_container_width=True,
)

with st.expander("Описание графика"):
    st.markdown(
        """
    Айтемы, популярность которых доминирует над остальными, также показывают специфичную
    динамику просмотров по неделям: они врываются резко и неожиданно. Такое поведение значительно меняет
    распределение просмотров на той или иной неделе и имеет большое влияние для построения
    схемы валидации и обучения двухэтапной модели. Надо аккуратно выбирать, на каком временном 
    диапазоне учить и валидировать модель.
    """
    )

st.plotly_chart(
    plot_time_series(
        top_100_weekly_series_df, "Динамика просмотров топ 100 сериалов по неделям"
    ),
    use_container_width=True,
)

with st.expander("Описание графика"):
    st.markdown(
        """
    В сериалах заметны свои "врывающиеся" хиты. Стоит заметить, что в датасете оставлено только
    одно взаимодействие юзера с айтемом - последнее по дате. Это означает, что вся долгая история
    просмотров каждой серии полностью стирается, а пики просмотров в датасете не являются пиками
    собственно популярности сериала. Это чаще всего время, близкое к выходу последней серии сезона - 
    то есть время, когда юзеры перестают смотреть какой-то сериал. Такие пики также значительно
    меняют распределение просмотров на определенной неделе, и это надо иметь ввиду во время валидации
    и обучения модели.
    """
    )
st.divider()

st.markdown(
    """
Больше информации о датасете можно узнать в статье ["MTS Kion Implicit Contextualised Sequential 
Dataset for Movie Recommendation"](https://arxiv.org/abs/2209.00325) с конференции  ACM RecSys CARS 
workshop 2022, в написании которой часть нашей команды принимала участие.
"""
)

st.divider()
st.markdown("Больше интересного:")

switch_online = st.button(":movie_camera: Онлайн рекомендации: двухэтапная модель")
if switch_online:
    switch_page("онлайн рекомендации")

switch_offline = st.button(
    ":books: Метрики и визуальный анализ: эксперименты с моделями"
)
if switch_offline:
    switch_page("Метрики и визуальный анализ")

switch_real = st.button(
    ":rocket: Детали реализации: двухэтапная модель, микросервисы, DVC"
)
if switch_real:
    switch_page("детали реализации")
