import logging
import os
import pathlib
import typing as tp
from copy import deepcopy
from datetime import datetime, timedelta
from enum import Enum
from pprint import pformat
from tempfile import TemporaryDirectory

import mlflow
import numpy as np
import pandas as pd
import yaml
from pydantic import BaseModel  # pylint: disable=no-name-in-module
from rectools import Columns
from rectools.dataset import Dataset, Interactions
from rectools.metrics import MAP, MeanInvUserFreq, Recall, Serendipity, calc_metrics
from rectools.metrics.base import MetricAtK
from rectools.model_selection import TimeRangeSplitter
from rectools.models import PopularModel
from rectools.models.base import ModelBase

from src.db.tools import create_engine_to_feature_db
from src.models.heuristics import CompleteModel
from src.models.implicit_online_model import save_and_log_implicit_online_model
from src.models.two_stage_recommender import TwoStageModel, TwoStageRecommender
from src.models.two_stage_tools import save_log_and_register_two_stage_online_model

SELECTED_RECO_USERS_PATH = "data/processed/chosen_for_showcase_users.csv"
WORKFLOW_CONFIG_PATH = "workflow/workflow_config.yaml"
MODEL_NAME_FROM_PARAMS = "from_params"


class MetricType(str, Enum):
    RECALL = "recall"
    MAP_ = "map"
    MIUF = "miuf"
    SERENDIPITY = "serendipity"
    SHARE_USERS_WITH_TOP_ITEM = "share_user_with_top_item"
    RECALL_NO_POP = "recall_no_pop"
    POP_INTERSECT = "pop_intersect"


class MetricDescription(BaseModel):
    metric_type: str
    params: tp.Dict[str, int]


def get_metrics_from_config(metrics_config: tp.Dict[str, MetricDescription]):
    metrics = {}
    for metric_name, metric_config in metrics_config.items():
        metric_description = MetricDescription.parse_obj(metric_config)
        metrics[metric_name] = get_metric_from_settings(metric_description)
    return metrics


def get_metric_from_settings(metric_setting: MetricDescription) -> MetricAtK:
    """Getting Rectools metric from config

    Parameters
    ----------
    metric_setting : MetricDescription
        Metric config for Rectools Recommender model

    Returns
    -------
    MetricAtK
        Rectools metric

    Raises
    ------
    ValueError
        Raises when metric type is not known
    """
    metric_type = metric_setting.metric_type
    metric_params = metric_setting.params

    if metric_type == MetricType.RECALL:
        metric = Recall(**metric_params)
    elif metric_type == MetricType.MAP_:
        metric = MAP(**metric_params)
    elif metric_type == MetricType.MIUF:
        metric = MeanInvUserFreq(**metric_params)
    elif metric_type == MetricType.SERENDIPITY:
        metric = Serendipity(**metric_params)
    else:
        raise ValueError(f"Metric type '{metric_type}' is not known")
    return metric


def mlflow_log_df_as_csv(df: pd.DataFrame, csv_file_name: str) -> None:
    with TemporaryDirectory() as temp_dir:
        file_name = f"{temp_dir}/{csv_file_name}"
        df.to_csv(file_name, index=False)
        mlflow.log_artifact(file_name)


def mlflow_log_dict_as_yaml(
    config_dict: tp.Dict[str, tp.Any], yaml_file_name: str
) -> None:
    with TemporaryDirectory() as temp_dir:
        file_name = f"{temp_dir}/{yaml_file_name}"
        with open(file_name, "w") as yaml_file:
            yaml_file.write(yaml.dump(config_dict, default_flow_style=False))
        mlflow.log_artifact(file_name)


def fit_model_with_cross_validation(
    model_obj: ModelBase,
    interactions_df: pd.DataFrame,
    splitter: TimeRangeSplitter,
    metrics: tp.Dict[str, MetricAtK],
    custom_metrics: tp.Optional[tp.Dict[str, MetricDescription]] = None,
    model_name: str = "model",
    k_recos: int = 10,
) -> tp.List[tp.Dict]:
    """Fits a model with cross-validation using the provided splitter and evaluates
    the model's performance using the specified metrics.

    Parameters
    ----------
    model : ModelBase
        Rectools model
    interactions_df: pd.DataFrame
        Interactions for Interactions rectool's object
    splitter : TimeRangeSplitter
        Splitter for cross-validation by time.
    metrics : tp.Dict[str, MetricAtK]
        Dictionary of rectool's metrics
    day_offset : int, optional
        The number of days to consider for reducing weights based on interaction age.
        If None, no penalty based on interaction age is applied. Default is None.
    interaction_offset : int, optional
        The number of interactions to consider for reducing weights based on interaction count.
        If None, no penalty based on interaction count is applied. Default is None.
    k_recos : int
        Amount of created recommendations for each user in test fold. Default is 10.

    Returns
    -------
    List[Dict]
        List of computed metrics per fold
    List[Dict]
        List of information about each fold
    """
    logger = logging.getLogger("MODEL CROSSVALIDATION")
    inters = Interactions(interactions_df)
    fold_iterator = splitter.split(inters, collect_fold_stats=True)
    cv_results = []

    for i_fold, (train_ids, test_ids, fold_info) in enumerate(fold_iterator):
        # Fold train/test split

        model = deepcopy(model_obj)

        logger.info(
            f"Start training for fold {i_fold}, Test start date: {fold_info['Start date'].date()}"
        )
        df_train = inters.df.iloc[train_ids]
        df_test = inters.df.iloc[test_ids]
        logger.info(f"Dataset max date: {df_train['datetime'].max().date()}")
        dataset = Dataset.construct(df_train)

        # Catalog is set of items that we recommend.
        catalog = df_train[Columns.Item].unique()

        # We will build recommendations for these users
        reco_users = np.unique(df_test[Columns.User])

        # Fit-recommend
        model.fit(dataset)
        recos = model.recommend(
            users=reco_users,
            dataset=dataset,
            k=k_recos,
            filter_viewed=True,
            add_rank_col=True,
        )

        metric_values = calc_metrics(
            metrics,
            reco=recos,
            interactions=df_test,
            prev_interactions=df_train,
            catalog=catalog,
        )
        if custom_metrics is not None:
            custom_metric_values = calc_custom_metrics(
                custom_metrics,
                reco=recos,
                interactions=df_test,
                prev_interactions=df_train,
            )
            metric_values.update(custom_metric_values)
        metric_values.update(
            {
                "fold": i_fold,
                "fold_start_date": fold_info["Start date"].date(),
                "fold_n_days": (fold_info["End date"] - fold_info["Start date"]).days,
                "model_name": model_name,
            }
        )
        cv_results.append(metric_values)
        logger.info(
            f"Current results for model {model_name}: \n \n"
            + pformat(
                pd.DataFrame(cv_results)
                .drop(columns=["model_name", "fold_start_date"])
                .round(5)
            )
            + "\n \n"
        )

    return cv_results


def get_popular_recos(prev_interactions: pd.DataFrame, users: np.ndarray, k: int):
    pop_recommender = PopularModel(period=timedelta(days=7))
    dataset = Dataset.construct(prev_interactions)
    pop_recommender.fit(dataset)
    pop_recos = pop_recommender.recommend(
        users=users,
        dataset=dataset,
        k=k,
        filter_viewed=True,
        add_rank_col=False,
    )
    return pop_recos


def calc_custom_metrics(
    custom_metrics: tp.Dict[str, MetricDescription],
    reco: pd.DataFrame,
    interactions: pd.DataFrame,
    prev_interactions: pd.DataFrame,
) -> tp.Dict[str, float]:
    metric_values = {}
    for metric_name, metric_config in custom_metrics.items():
        metric_description = MetricDescription.parse_obj(metric_config)
        if metric_description.metric_type == MetricType.SHARE_USERS_WITH_TOP_ITEM:
            top_item = (  # noqa: F841
                prev_interactions.groupby(Columns.Item)[Columns.User]
                .count()
                .sort_values(ascending=False)
                .head(1)
                .index[0]
            )
            k_reco = reco.query("rank <= @metric_description.params['k']")  # noqa: F841
            users_count = reco[Columns.User].nunique()
            users_with_top_item = reco.query("item_id == @top_item")[
                Columns.User
            ].nunique()
            metric_value = users_with_top_item / users_count if users_count > 0 else 0.0
            metric_values[metric_name] = metric_value
        elif metric_description.metric_type == MetricType.RECALL_NO_POP:
            pop_recos = get_popular_recos(
                prev_interactions,
                reco["user_id"].unique(),
                metric_description.params["k"],
            )
            pop_recos["in_pop"] = 1
            no_pop_test_interactions = interactions.merge(
                pop_recos.drop(columns="score"), on=["user_id", "item_id"], how="left"
            )
            no_pop_test_interactions = no_pop_test_interactions[
                no_pop_test_interactions["in_pop"].isna()
            ]
            no_pop_test_interactions.drop(columns=["in_pop"])
            recall = Recall(k=metric_description.params["k"])
            no_pop_reco = reco.merge(
                no_pop_test_interactions[["user_id"]].drop_duplicates(),
                on="user_id",
                how="inner",
            )
            metric_values[metric_name] = calc_metrics(
                {"recall": recall},
                reco=no_pop_reco,
                interactions=no_pop_test_interactions,
                prev_interactions=prev_interactions,
                catalog=prev_interactions["item_id"].unique(),
            )["recall"]
        elif metric_description.metric_type == MetricType.POP_INTERSECT:
            pop_recos = get_popular_recos(
                prev_interactions,
                reco["user_id"].unique(),
                metric_description.params["k"],
            )
            k_recos = reco[reco["rank"] <= metric_description.params["k"]]
            intersected_recos = k_recos.merge(pop_recos, on=["user_id", "item_id"])
            metric_values[metric_name] = intersected_recos.shape[0] / k_recos.shape[0]
    return metric_values


def get_splitter(
    last_date: np.datetime64, n_folds=3, unit: str = "W", n_units: int = 1
) -> TimeRangeSplitter:
    """Get TimeRangeSplitter with specified params

    Parameters
    ----------
    last_date : np.datetime64
        Last date for splitter
    n_folds : int, optional
        Number of folds, by default 3
    unit : str, optional
        Unit (`W` for week), by default "W"
    n_units : int, optional
        Number of units, by default 1

    Returns
    -------
    TimeRangeSplitter
        TimeRangeSplitter with specified params
    """
    # Left border will be included in fold, right will be excluded from fold
    start_date = last_date - pd.Timedelta(
        n_folds * n_units, unit=unit
    )  # Start date of first test fold
    periods = n_folds + 1
    freq = f"{n_units}{unit}"
    date_range = pd.date_range(start=start_date, periods=periods, freq=freq)

    splitter = TimeRangeSplitter(
        date_range=date_range,
        filter_already_seen=True,
        filter_cold_items=True,
        filter_cold_users=True,
    )
    return splitter


def read_config(filename: str) -> tp.Dict[str, tp.Any]:
    with open(filename, "r", encoding="utf-8") as file:
        config = yaml.load(file, Loader=yaml.FullLoader)
    return config


def get_users_for_visual_analysis() -> np.ndarray:
    users_for_visual_analysis_df = pd.read_csv(SELECTED_RECO_USERS_PATH)
    users_for_visual_analysis = users_for_visual_analysis_df["user_id"].unique()
    return users_for_visual_analysis


def save_recos_for_visual_analysis(
    fitted_model: ModelBase, model_name: str, dataset: Dataset
) -> None:

    showcase_users = get_users_for_visual_analysis()

    recos = fitted_model.recommend(
        users=showcase_users,
        dataset=dataset,
        k=10,
        filter_viewed=True,
    )
    recos["model"] = model_name
    recos["date"] = datetime.now().strftime("%Y-%m-%d")

    # Push them to db
    engine = create_engine_to_feature_db(ensure_db_exists=True)
    recos.to_sql(
        name=os.getenv("POSTGRES_RECOS_COMPARISON_TABLE"),
        con=engine,
        if_exists="append",
        method="multi",
        index=False,
    )
    engine.dispose()


def get_holdout_interactions() -> pd.DataFrame:
    workflow_config = read_config(WORKFLOW_CONFIG_PATH)
    cross_val_config = read_config(workflow_config["files"]["cv_config"])
    holdout_days = cross_val_config["params"]["HOLDOUT_DAYS"]
    interactions = pd.read_csv(
        workflow_config["files"]["clean_inter"], parse_dates=["datetime"]
    )
    last_date_before_holdout = interactions["datetime"].max() - timedelta(
        days=holdout_days
    )
    holdout_interactions = interactions[
        interactions["datetime"] > last_date_before_holdout
    ]
    return holdout_interactions


def save_model_results_on_full_df(
    model: ModelBase,
    full_df: pd.DataFrame,
    model_name: str,
    save_for_visual_analysis: bool,
    save_to_mlflow: bool,
    eval_on_holdout: bool,
    mlflow_yaml_artifacts: tp.Optional[tp.List[tp.Any]] = None,
):

    dataset = Dataset.construct(full_df)
    model.fit(dataset)

    if save_for_visual_analysis:
        save_recos_for_visual_analysis(
            fitted_model=model, model_name=model_name, dataset=dataset
        )

    if eval_on_holdout:
        holdout_interactions = get_holdout_interactions()
        logger = logging.getLogger("EVALUATION")
        logger.info(
            f"""
            Holdout interactions first date: {holdout_interactions['datetime'].min()}
            last date: {holdout_interactions['datetime'].max()}"""
        )
        logger.info(f"Train dataset last date: {full_df['datetime'].max()}")
        eval_users = (
            holdout_interactions[["user_id"]]
            .drop_duplicates()
            .merge(full_df[["user_id"]].drop_duplicates(), on="user_id", how="inner")[
                "user_id"
            ]
        )
        eval_interactions = holdout_interactions[
            holdout_interactions["user_id"].isin(eval_users)
        ]
        eval_recos = model.recommend(
            users=eval_users, dataset=dataset, filter_viewed=True, k=10
        )
        workflow_config = read_config(WORKFLOW_CONFIG_PATH)
        cross_val_config = read_config(workflow_config["files"]["cv_config"])
        metrics = get_metrics_from_config(cross_val_config["metrics"])
        metric_values = calc_metrics(
            metrics,
            reco=eval_recos,
            interactions=eval_interactions,
            prev_interactions=full_df,
            catalog=full_df[Columns.Item].unique(),
        )

    if save_to_mlflow:
        started_run = None
        if mlflow.active_run() is None:
            mlflow.set_experiment("Production registry")
            started_run = mlflow.start_run(description="Experiment with model saving")

        if metric_values is not None:
            mlflow.log_metrics(metric_values)

        if mlflow_yaml_artifacts is not None:
            for art in mlflow_yaml_artifacts:
                mlflow_log_dict_as_yaml(art, "two_stage_config.yaml")

        if isinstance(model, (TwoStageRecommender, TwoStageModel)):
            recommender = model.model if isinstance(model, TwoStageModel) else model
            model_info = save_log_and_register_two_stage_online_model(
                recommender=recommender,
                dataset_full=dataset,
                registry_model_name=model_name,
            )
        else:
            if isinstance(model, CompleteModel):
                rectools_model = model.model
                mlflow_log_dict_as_yaml(
                    {str(key): value for key, value in model.heuristics.items()},
                    "heuristics.yaml",
                )
            else:
                rectools_model = model
            pathlib.Path("models/base").mkdir(parents=True, exist_ok=True)
            model_info = save_and_log_implicit_online_model(
                rectools_model=rectools_model,
                model_path=f"models/base/{model_name}",
                item_id_map_external_ids=dataset.item_id_map.external_ids,
                registry_model_name=model_name,
            )
        if started_run:
            mlflow.end_run()

        return model_info
