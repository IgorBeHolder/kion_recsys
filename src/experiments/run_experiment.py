"""
Cross-validation on single two-stage model using existing two_stage_config
"""

import logging
import logging.config
import os
import typing as tp
from copy import deepcopy
from datetime import datetime
from pprint import pformat

import click
import mlflow
import pandas as pd
from dotenv import load_dotenv
from rectools import Columns
from rectools.models.base import ModelBase

from src.experiments.tools import (
    MODEL_NAME_FROM_PARAMS,
    WORKFLOW_CONFIG_PATH,
    fit_model_with_cross_validation,
    get_metrics_from_config,
    get_splitter,
    mlflow_log_df_as_csv,
    read_config,
    save_model_results_on_full_df,
)
from src.models import two_stage_blocks
from src.models.heuristics import (
    CompleteModel,
    Heuristic,
    get_complete_model_from_settings,
)
from src.models.tools import ModelDescription, get_model_name_from_settings
from src.models.two_stage_blocks import train_catboost_defined
from src.models.two_stage_recommender import TwoStageModel
from src.models.two_stage_tools import (
    mlflow_log_two_stage_config,
    parse_two_stage_config,
)

load_dotenv()


# ----------------------------- Helper functions -----------------------------#


def mlflow_run_for_model_cv(
    model_obj: ModelBase,
    model_name: str,
    nested: bool,
    full_df: pd.DataFrame,
    splitter,
    metrics,
    cross_val_config,
    exp_config,
    log_params: tp.Optional[tp.Dict[str, tp.Any]] = None,
) -> tp.Tuple[tp.List[tp.Dict[tp.Any, tp.Any]], tp.Dict[str, tp.Union[str, int]]]:

    dt_string = datetime.now().strftime("%Y-%m-%d %H:%M")
    if nested or (not nested and mlflow.active_run() is None):
        mlflow.start_run(
            run_name=f"{model_name} {dt_string}",
            nested=nested,
            description=exp_config["logging"]["run_description"],
        )

    cv_results = fit_model_with_cross_validation(
        model_obj=model_obj,
        interactions_df=full_df,
        splitter=splitter,
        metrics=metrics,
        model_name=model_name,
        k_recos=cross_val_config["params"]["K_RECOS"],
        custom_metrics=cross_val_config["custom_metrics"],
    )

    mlflow_log_df_as_csv(pd.DataFrame(cv_results).round(5), "model_cv_report.csv")
    mlflow.log_param("model", model_name)
    if isinstance(model_obj, CompleteModel):
        mlflow.log_params(model_obj.heuristics)

    # Log each fold metrics as steps
    all_metrics = {}
    all_metrics.update(metrics)
    all_metrics.update(cross_val_config["custom_metrics"])
    for fold_results in cv_results:
        for metric_name in list(all_metrics.keys()):
            mlflow.log_metric(
                f"{metric_name}_steps",
                fold_results[metric_name],
                fold_results["fold"],
            )

    # Log average metrics
    avg_metrics = pd.DataFrame(cv_results)[list(all_metrics.keys())].mean().to_dict()
    mlflow.log_metrics(avg_metrics)
    avg_report: tp.Dict[str, tp.Union[str, int]] = {"model": model_name}
    avg_report.update(avg_metrics)
    avg_report.update(
        {
            "n_folds": len(cv_results),
            "mlflow_run_id": mlflow.active_run().info.run_id,
        }
    )
    if log_params is not None:
        mlflow.log_params(log_params)

    if nested or not exp_config["two_stage"]:
        mlflow.end_run()

    return cv_results, avg_report


# ----------------------------------------------------------------------------#


@click.command()
@click.option(
    "-c",
    "--exp-config-path",
    type=click.Path(dir_okay=False),
    help="Path to experiment config YAML file",
)
def run_experiment(exp_config_path: str) -> None:
    """
    Run cross-validation comparison between models from YAML config file
    """
    logging.config.fileConfig("workflow/logging.conf")
    logger = logging.getLogger("EXPERIMENT")
    logger.info("Reading configs")

    # Read yaml, set up project constants, models and metrics
    workflow_config = read_config(WORKFLOW_CONFIG_PATH)
    cross_val_config = read_config(workflow_config["files"]["cv_config"])
    exp_config = read_config(exp_config_path)
    metrics = get_metrics_from_config(cross_val_config["metrics"])

    # Prepare mlflow experiment
    experiment = mlflow.set_experiment(exp_config["logging"]["experiment_name"])
    experiment_id = experiment.experiment_id

    # Prepare source data
    full_df = pd.read_csv(
        filepath_or_buffer=workflow_config["files"]["clean_inter"],
        parse_dates=["datetime"],
    )
    full_df[Columns.Weight] = 1

    first_holdout_date = full_df["datetime"].max() - pd.Timedelta(
        days=cross_val_config["params"]["HOLDOUT_DAYS"] - 1
    )
    logger.info(f"First holdout date: {first_holdout_date}")

    logger.info("Preparing models")
    # Prepare models
    if exp_config["two_stage"]:
        two_stage_config = parse_two_stage_config(exp_config_path)
        model_name = exp_config["logging"]["model_name"]
        pop_quantile = (
            two_stage_config["sampling"]["pop_quantile"]
            if "pop_quantile" in two_stage_config["sampling"]
            else None
        )
        if (
            "blocks" in two_stage_config
            and "features_func" in two_stage_config["blocks"]
        ):
            calc_features_func = getattr(
                two_stage_blocks, two_stage_config["blocks"]["features_func"]
            )
        else:
            calc_features_func = getattr(
                two_stage_blocks, "merge_and_select_features_defined"
            )
        model = TwoStageModel(
            first_stage_models_config=two_stage_config["first_stage_config"],
            sampling_config=two_stage_config["sampling"],
            training_func=train_catboost_defined,
            calc_features_func=calc_features_func,
            extra={
                "features_config": two_stage_config["features"],
                "catboost_params": two_stage_config["catboost_params"],
                "pop_quantile": pop_quantile,
            },
        )
        models = {model_name: model}
        log_params = {model_name: None}
    else:
        models = {}
        log_params = {}
        for model_name, model_config in exp_config["models"].items():
            model_description = ModelDescription.parse_obj(
                model_config["model_description"]
            )
            if model_name == MODEL_NAME_FROM_PARAMS:
                model_name = get_model_name_from_settings(model_description)
            log_params[model_name] = model_config["model_description"]["params"]

            heuristics = {
                Heuristic(key): value
                for key, value in model_config["heuristics"].items()
            }
            models[model_name] = get_complete_model_from_settings(
                model_description, heuristics
            )
        if (
            exp_config["logging"]["nested_runs"]
            and not exp_config["logging"]["skip_metrics"]
        ):
            mlflow.start_run(
                run_name=datetime.now().strftime("%Y-%m-%d"),
                description=exp_config["logging"]["run_description"],
            )
    # Get model results on full data: save model and/or save recos for visual analysis
    if (
        exp_config["logging"]["save_for_visual_analysis"]
        or exp_config["logging"]["save_to_mlflow"]
    ):
        logger.info("Prepaing result on full dataset")
        for model_name, model_obj in models.items():
            no_holdout_df = full_df[full_df["datetime"] < first_holdout_date]
            model = deepcopy(model_obj)
            mlflow_yaml_artifacts = (
                [exp_config["two_stage_config"]] if exp_config["two_stage"] else None
            )
            save_model_results_on_full_df(
                model=model,
                full_df=no_holdout_df,
                model_name=model_name,
                save_for_visual_analysis=exp_config["logging"][
                    "save_for_visual_analysis"
                ],
                save_to_mlflow=exp_config["logging"]["save_to_mlflow"],
                eval_on_holdout=exp_config["logging"]["save_to_mlflow"],
                mlflow_yaml_artifacts=mlflow_yaml_artifacts,
            )
    if exp_config["logging"]["skip_metrics"]:
        logger.info("Skipping metrics calculation on cv as specified in config")
        return

    logger.info("Starting cross validation")
    # Set up CV splitter
    splitter = get_splitter(
        last_date=first_holdout_date,
        n_folds=cross_val_config["params"]["N_FOLDS"],
        n_units=cross_val_config["params"]["N_DAYS_FOLD"],
        unit="D",
    )

    avg_reports = []
    full_reports = []
    nested = (not exp_config["two_stage"]) and exp_config["logging"]["nested_runs"]
    for model_name, model in models.items():
        cv_results, avg_report = mlflow_run_for_model_cv(
            model_obj=model,
            model_name=model_name,
            nested=nested,
            full_df=full_df,
            splitter=splitter,
            metrics=metrics,
            cross_val_config=cross_val_config,
            exp_config=exp_config,
            log_params=log_params[model_name],
        )
        avg_reports.append(avg_report)
        full_reports.append(cv_results)

    avg_reposrts_df = pd.DataFrame(avg_reports).round(5)
    avg_reposrts_df["experiment_id"] = experiment_id
    logger.info(
        "Mean metrics per model \n \n"
        + pformat(avg_reposrts_df.drop(columns=["mlflow_run_id", "experiment_id"]))
    )
    report_filename = exp_config["logging"]["report_filename"]
    report_file_mode = exp_config["logging"]["report_file_mode"]
    write_header = not os.path.exists(report_filename) or report_file_mode == "w"
    avg_reposrts_df.to_csv(
        report_filename, index=False, mode=report_file_mode, header=write_header
    )

    if exp_config["two_stage"]:
        mlflow_log_two_stage_config(two_stage_config)

    if exp_config["two_stage"] or exp_config["logging"]["nested_runs"]:
        mlflow.log_artifact(report_filename)

        mlflow_log_df_as_csv(
            pd.concat([pd.DataFrame(report) for report in full_reports], axis=0).round(
                5
            ),
            "all_models_cv.csv",
        )
        mlflow.log_artifact(exp_config_path)
        mlflow.log_artifact(workflow_config["files"]["cv_config"])

        mlflow.end_run()


if __name__ == "__main__":
    run_experiment()
