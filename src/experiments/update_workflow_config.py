import logging
import logging.config
import pathlib
from datetime import datetime, timedelta

import pandas as pd
import yaml
from rectools.model_selection.time_split import TimeRangeSplitter
from rectools.utils import pairwise

from src.experiments.tools import WORKFLOW_CONFIG_PATH, get_splitter, read_config


def main():
    logging.config.fileConfig("workflow/logging.conf")
    logger = logging.getLogger("FEATURE_DATES_UPDATE")

    workflow_config = read_config(WORKFLOW_CONFIG_PATH)
    cross_val_config = read_config(workflow_config["files"]["cv_config"])

    train_period_n_days_vars = set()
    two_stage_configs_paths = list(
        pathlib.Path("src/experiments/two_stage").glob(r"*.yaml")
    )
    for two_stage_config_path in two_stage_configs_paths:
        two_stage_exp_config = read_config(two_stage_config_path)
        train_period_n_days_vars.add(
            two_stage_exp_config["two_stage_config"]["sampling"]["train_period_n_days"]
        )
    logger.info(f"Two-stage train_period_n_days_vars: {train_period_n_days_vars}")

    interactions_df = pd.read_csv(
        filepath_or_buffer=workflow_config["files"]["clean_inter"],
        parse_dates=["datetime"],
    )
    last_date_of_inter = interactions_df["datetime"].max()
    last_date_of_cv = last_date_of_inter - pd.Timedelta(
        days=(cross_val_config["params"]["HOLDOUT_DAYS"] - 1)
    )
    logger.info(
        f"""
    Last date of interactions: {last_date_of_inter.date()} \n
    Last date of cv: {last_date_of_cv.date()} (not included) \n
    HOLDOUT_DAYS: {cross_val_config["params"]["HOLDOUT_DAYS"]}
    """
    )
    splitter = get_splitter(
        last_date=last_date_of_cv,
        n_folds=cross_val_config["params"]["N_FOLDS"],
        n_units=cross_val_config["params"]["N_DAYS_FOLD"],
        unit="D",
    )

    dates_for_features = (
        set(workflow_config["features_dates"])
        if workflow_config["features_dates"] is not None
        else set()
    )
    date_range = TimeRangeSplitter._get_real_date_range(
        interactions_df["datetime"], splitter.date_range
    )

    for i_fold, (test_start_date, test_end_date) in enumerate(pairwise(date_range)):
        logger.info(
            f"""
        Fold {i_fold} test start: {test_start_date.date()} test end (not included): {test_end_date.date()}
        num_days: {(test_end_date-test_start_date).days}"""
        )
        date_for_feature_calculation = test_start_date - pd.Timedelta(days=1)
        dates_for_features.add(
            datetime.strftime(date_for_feature_calculation, "%Y-%m-%d")
        )
        for train_period_n_days in train_period_n_days_vars:
            dates_for_features.add(
                datetime.strftime(
                    date_for_feature_calculation - timedelta(days=train_period_n_days),
                    "%Y-%m-%d",
                )
            )

    workflow_config["features_dates"] = sorted(list(dates_for_features))

    with open(WORKFLOW_CONFIG_PATH, "w") as yaml_file:
        yaml_file.write(yaml.dump(workflow_config, default_flow_style=False))


if __name__ == "__main__":
    main()
