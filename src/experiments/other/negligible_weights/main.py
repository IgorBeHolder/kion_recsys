"""
Testing the hypothesis that setting the weights of old interactions closer to zero
will exclude them from the interaction history and improve the metrics on the test sample.
Based on: https://rectools.readthedocs.io/en/latest/examples/3_metrics.html
"""

import copy
import itertools
import os

import mlflow
import pandas as pd
from dotenv import load_dotenv
from implicit import nearest_neighbours as NN
from rectools import Columns
from rectools.dataset import Dataset
from rectools.metrics import MAP, MeanInvUserFreq, Recall, calc_metrics
from rectools.models import ImplicitItemKNNWrapperModel

load_dotenv()

INTERACTIONS_DATA_PATH = "data/interim/interactions_clean.csv"  # Interaction data
TRAIN_DAYS_OFFSET = 170  # Lower days cut-off threshold for training and prediction.
TRAIN_TEST_DAYS_OFFSET = (
    7  # Lower days cut-off threshold for prediction and metric calculation.
)
INTERACTIONS_LIMIT = 20  # Interactions above this limit will get negligible weights
NEGLIGIBLE_WEIGHT = 1e-6  # Negligible weight value
K_NEIGHBOURS = 200  # Neighbours amount for KNN

# Initialize MLflow run
mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("weights_penalty_in_recs")
mlflow.start_run()

# Log constants
mlflow.log_param("TRAIN_DAYS_OFFSET", TRAIN_DAYS_OFFSET)
mlflow.log_param("TRAIN_TEST_DAYS_OFFSET", TRAIN_TEST_DAYS_OFFSET)
mlflow.log_param("INTERACTIONS_LIMIT", INTERACTIONS_LIMIT)
mlflow.log_param("NEGLIGIBLE_WEIGHT", NEGLIGIBLE_WEIGHT)
mlflow.log_param("K_NEIGHBOURS", K_NEIGHBOURS)


# Prepare source data
raw_df = pd.read_csv(
    filepath_or_buffer=INTERACTIONS_DATA_PATH,
    usecols=["user_id", "item_id", "datetime"],
    parse_dates=["datetime"],
)
raw_df = raw_df.rename(
    columns={
        "user_id": Columns.User,
        "item_id": Columns.Item,
        "datetime": Columns.Datetime,
    }
)

# Train/test split
split_date = raw_df["datetime"].max() - pd.DateOffset(days=TRAIN_TEST_DAYS_OFFSET)
df_train = raw_df.loc[raw_df["datetime"] < split_date]
df_test = raw_df.loc[raw_df["datetime"] >= split_date]


# Select common users for recommendations
reco_users = set(df_train["user_id"]).intersection(set(df_test["user_id"]))

# Calculate weights
df_train_negl = df_train.copy()
df_train_ones = df_train.copy()

# set penalty by interactions amount
df_train_negl["interaction_count"] = (
    df_train_negl.sort_values(by=["datetime"], ascending=False)
    .groupby("user_id")
    .cumcount()
    + 1
)
df_train_negl[Columns.Weight] = 1
df_train_negl.loc[
    df_train_negl["interaction_count"] >= INTERACTIONS_LIMIT, Columns.Weight
] = NEGLIGIBLE_WEIGHT

# set penalty by event age
minimal_date = raw_df["datetime"].max() - pd.DateOffset(days=TRAIN_DAYS_OFFSET)
df_train_negl.loc[
    df_train_negl["datetime"] < minimal_date, Columns.Weight
] = NEGLIGIBLE_WEIGHT

df_train_ones[Columns.Weight] = 1

# Create Datasets
datasets = {
    "ones": Dataset.construct(df_train_ones),
    "negl": Dataset.construct(df_train_negl),
}

# Metrics set up
metrics = {
    "recall@5": Recall(k=5),
    "recall@10": Recall(k=10),
    "recall@20": Recall(k=20),
    "miuf10@10": MeanInvUserFreq(k=10),
    "map@20": MAP(k=20),
}

model_objects = {
    "KNN": ImplicitItemKNNWrapperModel(NN.CosineRecommender(K=K_NEIGHBOURS), verbose=1),
    "TF-IDF": ImplicitItemKNNWrapperModel(
        NN.TFIDFRecommender(K=K_NEIGHBOURS), verbose=1
    ),
}


def get_metrics(model_obj, train_dataset, recommend_dataset):
    model = copy.deepcopy(model_obj)
    model.fit(train_dataset)
    recos = model.recommend(
        users=reco_users,
        dataset=recommend_dataset,
        k=20,
        filter_viewed=True,
    )
    catalog = df_train[Columns.Item].unique()
    metric_results = calc_metrics(
        metrics,
        reco=recos,
        interactions=df_test,
        prev_interactions=df_train,
        catalog=catalog,
    )
    return metric_results


metrics_results = {}
loop_params = [model_objects.keys(), datasets.keys(), datasets.keys()]
for model_key, train_data_key, reco_data_key in itertools.product(*loop_params):
    experiment_key = f"{model_key}-{train_data_key}-{reco_data_key}"
    print(experiment_key)
    metrics_results[experiment_key] = get_metrics(
        model_objects[model_key], datasets[train_data_key], datasets[reco_data_key]
    )

print("Result metrics table")
print(pd.DataFrame.from_dict(metrics_results).T)

# Log metric results
mlflow.log_dict(metrics_results, "metrics.json")
mlflow.end_run()
