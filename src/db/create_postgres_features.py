"""
Create CSV file with item features for storing it in DB
"""
import logging

import click
import pandas as pd


def filter_prioritized_items(row, priproty_order, black_list=set([])):
    items = row.split(",")
    for item in items:
        if (item in priproty_order) and (item not in black_list):
            return item


@click.command()
@click.argument("items_time_features_input_path", type=click.Path())
@click.argument("items_static_features_input_path", type=click.Path())
@click.argument("items_clean_input_path", type=click.Path())
@click.argument("items_output_path", type=click.Path())
def create_postgres_features(
    items_time_features_input_path: str,
    items_static_features_input_path: str,
    items_clean_input_path: str,
    items_output_path: str,
) -> None:
    """
    Merge static and time-based items feature for storing in Postgres.
    Add general items info: title, country, genres, etc.
    Save result to CSV file, ready for export to data base.

    Args:
    - items_time_features_input_path (str): path to time-based item features csv
    - items_static_features_input_path (str): path to static item features csv
    - items_clean_input_path (str): path to cleaned items csv
    - items_output_path (str): path for output csv file.

    Return:
    - None

    """
    logging.basicConfig(level=logging.INFO)
    logging.info("Create static item features")

    time_features_df = pd.read_csv(items_time_features_input_path)
    static_features_df = pd.read_csv(items_static_features_input_path)
    clean_items_df = pd.read_csv(
        items_clean_input_path,
        usecols=[
            "item_id",
            "title",
            "title_orig",
            "countries",
            "genres",
            "release_year",
        ],
    )

    result_df = time_features_df.merge(static_features_df, how="outer", on="item_id")
    result_df = result_df.merge(clean_items_df, how="left", on="item_id")

    assert (
        time_features_df.isna().sum().sum() == 0
    ), "Detect NaNs in item time-based features subset"
    assert (
        static_features_df.isna().sum().sum() == 0
    ), "Detect NaNs in item static features subset"
    assert (
        result_df.isna().sum().sum() == 0
    ), "Inconsistent item IDs in features subsets"

    # Find the most poluar country for each item
    priority_country = (
        pd.melt(result_df["countries"].str.split(expand=True))["value"]
        .dropna()
        .value_counts()
        .index.to_list()
    )
    result_df["main_country"] = result_df["countries"].apply(
        lambda x: filter_prioritized_items(x, priority_country)
    )

    # Find the most poluar genre for each item
    priority_genre = (
        pd.melt(result_df["genres"].str.split(expand=True))["value"]
        .dropna()
        .value_counts()
        .index.to_list()
    )
    black_list_genre = set(["русские", "зарубежные"])
    result_df["main_genre"] = result_df["genres"].apply(
        lambda x: filter_prioritized_items(x, priority_genre, black_list_genre)
    )
    result_df.loc[:, ["main_country", "main_genre"]] = result_df.loc[
        :, ["main_country", "main_genre"]
    ].fillna("None")

    result_df.to_csv(items_output_path, index=False)


if __name__ == "__main__":
    create_postgres_features()
