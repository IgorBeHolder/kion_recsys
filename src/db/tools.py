import logging
import os

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy_utils import create_database, database_exists


def create_engine_to_feature_db(ensure_db_exists: bool = False) -> Engine:
    load_dotenv()

    engine = create_engine(
        f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}'
        f'@{os.getenv("DB_HOST")}:5432/{os.getenv("POSTGRES_FEATURE_DB")}'
    )
    if ensure_db_exists and not database_exists(engine.url):
        create_database(engine.url)
        logging.info("No database detected. Database was created.")

    return engine
