"""
Create CSV file with weekly item features for storing it in DB
"""
import logging

import click
import pandas as pd


@click.command()
@click.argument("interactions_input_path", type=click.Path())
@click.argument("items_input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def create_postgres_weekly_features(
    interactions_input_path: str,
    items_input_path: str,
    output_path: str,
) -> None:
    logging.basicConfig(level=logging.INFO)
    logging.info("Create weekly postgres features")

    interaction_df = pd.read_csv(interactions_input_path, parse_dates=["datetime"])
    items_df = pd.read_csv(items_input_path)

    interaction_df["week"] = interaction_df["datetime"].dt.to_period("W").dt.start_time

    by_week = interaction_df.pivot_table(
        index=["item_id"],
        columns="week",
        values="user_id",
        aggfunc="count",
        fill_value=0,
    )
    by_week = pd.merge(
        by_week,
        items_df[["item_id", "title", "content_type", "release_year"]],
        left_index=True,
        right_on="item_id",
    )
    result_df = pd.melt(
        by_week,
        id_vars=["item_id", "title", "content_type", "release_year"],
        var_name=["week"],
        value_name="cnt",
    )
    result_df = result_df.sort_values(by=["title", "week"], ascending=True)
    result_df.to_csv(output_path, index=False)


if __name__ == "__main__":
    create_postgres_weekly_features()
