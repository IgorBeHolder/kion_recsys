import click
import yaml

from src.visualization.showcase import VisualBenchmark

SELECTED_MODELS_YAML_PATH = "src/visualization/showcase_models.yaml"
N_RANDOM_USERS = 0


@click.command()
@click.argument("showcase_output_folder", type=click.Path())
def create_and_save_showcase_data(showcase_output_folder: str):
    with open(SELECTED_MODELS_YAML_PATH, "r", encoding="utf-8") as file:
        selected_models = yaml.load(file, Loader=yaml.FullLoader)
    current_showcase = VisualBenchmark.show_current_recos(
        models_to_show=list(selected_models.keys()),
        auto_display=False,
        n_add_random_users=N_RANDOM_USERS,
    )
    current_showcase.full_recos["model"] = current_showcase.full_recos["model"].replace(
        selected_models
    )
    # delete duplicates
    current_showcase.full_recos = current_showcase.full_recos.drop_duplicates(
        subset=["model", "item_id", "user_id", "rank"]
    )

    current_showcase.save_data(
        showcase_folder_name=showcase_output_folder, name="prod", force_overwrite=True
    )


if __name__ == "__main__":
    create_and_save_showcase_data()
