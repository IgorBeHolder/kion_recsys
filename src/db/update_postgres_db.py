"""
Create if not existed or rewrite `features` table in PostreSQL DB by input csv file
"""
import logging
import os

import click
import pandas as pd
from dotenv import load_dotenv

from src.db.tools import create_engine_to_feature_db


@click.command()
@click.option(
    "--csv-path",
    type=click.Path(exists=True),
    help="The file path to the CSV file containing the data to replace the table with.",
)
@click.option(
    "--table-name",
    type=str,
    help="The file path to the CSV file containing the data to replace the table with.",
)
def replace_postgres_table_with_csv(csv_path: str, table_name: str) -> None:
    """
    Replace the contents of a PostgreSQL table with the data from a CSV file.
    Use table names from .env file.

    Args:
    - csv_path (str): The file path to the CSV file containing the data to replace the table with.
    - table_name (str): Target name of Postgres table from .env file
    Returns:
    - None
    """
    logging.basicConfig(level=logging.INFO)
    load_dotenv()

    actual_table_name = os.getenv(table_name)
    if actual_table_name is None:
        raise ValueError(
            f"Environment variable {table_name} doesn't exist. Check the .env file"
        )

    df = pd.read_csv(csv_path)
    engine = create_engine_to_feature_db(ensure_db_exists=True)

    df.to_sql(
        name=actual_table_name,
        con=engine,
        if_exists="replace",
        method="multi",
        chunksize=10000,
    )

    logging.info("Successfully updated PostreSQL table.")

    engine.dispose()


if __name__ == "__main__":
    replace_postgres_table_with_csv()
