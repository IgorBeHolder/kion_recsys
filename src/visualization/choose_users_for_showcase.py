import click
import pandas as pd
import yaml


@click.command()
@click.argument("showcase_users_config_path", type=click.Path())
@click.argument("selected_users_output_path", type=click.Path())
def choose_users_for_showcase(
    showcase_users_config_path: str,
    selected_users_output_path: str,
) -> None:

    with open(showcase_users_config_path, "r", encoding="utf-8") as yamlfile:
        named_users = yaml.load(yamlfile, Loader=yaml.FullLoader)
    named_users = pd.Series(list(named_users.values()))
    named_users.name = "user_id"
    named_users.to_csv(selected_users_output_path, index=False)


if __name__ == "__main__":
    choose_users_for_showcase()
