SOURCES=src
CURRENT_TWO_STAGE_EXPERIMENT_CONFIG_PATH ="src/experiments/two_stage/v0_competition_winner.yaml"
CURRENT_BASELINE_EXPERIMENT_CONFIG_PATH ="src/experiments/baselines/current.yaml"

all:
	@echo "Running isort..."
	@isort ${SOURCES}
	@echo "Running black..."
	@black ${SOURCES}/*/*.py
	@black ${SOURCES}/*/*/*.py

lint:
	@echo "Running black check..."
	@black --check --diff ${SOURCES}/*/*.py
	@echo "Running pylint..."
	@pylint ${SOURCES}
	@echo "Running mypy..."
	@mypy ${SOURCES}
	@echo "Running flake8..."
	@flake8 ${SOURCES}

prod:
	@echo "Running data pipeline..."
	@dvc repro
	@echo "Running production training pipeline..."
	@dvc repro -R workflow/prod

exp_two:
	@echo "Checking cross_val scheme..."
	@python src/experiments/update_workflow_config.py
	@echo "Running data pipeline..."
	@dvc repro
	@echo "Running experiment..."
	@python src/experiments/run_experiment.py --exp-config-path ${CURRENT_TWO_STAGE_EXPERIMENT_CONFIG_PATH}
	
exp_base:
	@echo "Checking cross_val scheme..."
	@python src/experiments/update_workflow_config.py
	@echo "Running data pipeline..."
	@dvc repro
	@echo "Running experiment..."
	@python src/experiments/run_experiment.py --exp-config-path ${CURRENT_BASELINE_EXPERIMENT_CONFIG_PATH}
